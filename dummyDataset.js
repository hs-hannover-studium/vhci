const fs = require('fs');

const states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"];

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomString(length) {
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let result = '';
  for (let i = 0; i < length; i++) {
    result += chars.charAt(getRandomInt(0, chars.length - 1));
  }
  return result;
}

function getRandomDate(start, end) {
  const date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');
  return `${year}-${month}-${day}`;
}

function calculateAge(birthDate) {
  const birth = new Date(birthDate);
  const now = new Date();
  let age = now.getFullYear() - birth.getFullYear();
  const m = now.getMonth() - birth.getMonth();
  if (m < 0 || (m === 0 && now.getDate() < birth.getDate())) {
    age--;
  }
  return age;
}

function getRandomState() {
  return states[getRandomInt(0, states.length - 1)];
}

function getRandomGender() {
  const genders = ["Male", "Female", "Non-Binary"];
  return genders[getRandomInt(0, genders.length - 1)];
}

function generateUserData() {
  const user_id = getRandomInt(1000000, 9999999);
  const username = getRandomString(10);
  const state = getRandomState();
  const gender = getRandomGender();
  const birth_date = getRandomDate(new Date(1950, 0, 1), new Date(2010, 0, 1));
  const age = calculateAge(birth_date);

  const genres = [];
  for (let i = 0; i < 45; i++) {
    genres.push(getRandomInt(0, 12));
  }

  return [user_id, username, state, gender, birth_date, age, ...genres].join(',');
}

function generateData(row) {
  const header = "user_id,username,state,gender,birth_date,age,Action,Adventure,Cars,Comedy,Dementia,Demons,Drama,Ecchi,Fantasy,Game,Harem,Hentai,Historical,Horror,Josei,Kids,Magic,Martial Arts,Mecha,Military,Music,Mystery,Parody,Police,Psychological,Romance,Samurai,School,Sci-Fi,Seinen,Shoujo,Shoujo Ai,Shounen,Shounen Ai,Slice of Life,Space,Sports,Super Power,Supernatural,Thriller,Vampire,Yaoi,Yuri";
  const data = [];
  data.push(header);
  for (let i = 0; i < row; i++) {
    data.push(generateUserData());
  }
  return data;
}

function saveToCSV(filename, data) {
  const csvContent = data.join('\n');
  fs.writeFileSync(filename, csvContent, 'utf8');
}


function userLocationCound(userList) {
  const locationCount = {};
  for (let i = 1; i < userList.length; i++) {
    const location = userList[i].split(',')[2];
    if (locationCount[location]) {
      locationCount[location]++;
    } else {
      locationCount[location] = 1;
    }
  }
  return Object.entries(locationCount).map(([location, count]) => `${location},${count}`);
}

const data = generateData(12000);
const locationCount = userLocationCound(data);

const dataDir = 'src/assets/data/Test';
saveToCSV(dataDir + '/users.csv', data);
saveToCSV(dataDir + '/locations.csv', locationCount);
