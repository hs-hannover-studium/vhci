﻿Project Title: WHO'S YOUR TYPICAL ANIME WATCHER?


Team Members: 
* Diem-Mi Nguyen,
* Julian Tolstich


Visualization Techniques
* Bar Chart
* Box Plot Chart
* Radar Chart
* Map with Color Mapping (log scale with fixed number of colors)


Description of usage:


Installation:
This projects requires a node Version of ^18.13.0 || ^20.9.0
To set up the project, first download it, then execute `npm install` to install the dependencies. To start the development server, run `ng serve`. You can access the application by navigating to `http://localhost:4200/` in your browser.


Project Description + User Interaction:
This project is a dashboard designed to analyze data related to anime consumption in the United States. It provides several interactive charts to explore different aspects of anime fandom:


Fans by State:
   - Displays the number of anime fans in each state.
   - Interactions include hovering over a state to see fan numbers, clicking to filter data by state, searching for a state, and adjusting data granularity.
   - Goal: Understand geographical distribution of anime fans.


Gender Count:
   - Shows gender distribution within the dataset or for a selected state.
   - Interactions involve clicking on gender images to filter data by gender.
   - Goal: Analyze gender demographics among anime fans.


Age Distribution by Gender:
   - Illustrates age distribution across genders, displaying minimum, maximum, and median ages.
   - No direct interactions.
   - Goal: Gain insights into the age demographics of anime fans.


Top Genres:
   - Lists the most watched anime genres based on applied filters (state and gender).
   - Updates dynamically with changes in filters.
   - Interactions include adding or removing genres and locking genres to maintain genre selection across filter changes.
   - Goal: Identify favorite anime genres among fans.


Additionally, the dashboard features "tomo-chan," an assistant who provides a summary for each chart.


Overall, the project aims to provide a comprehensive understanding of anime consumption trends, including geographic, demographic (gender and age), and genre preferences among fans in the United States.


Additional Things:
* Different Data Sets and Themes, which can be selected
* Usage of LocalStorage to store selected Data Set and Theme by the user
* Loading Indicator / Loading State of application
* Transitions in every Chart
* High focus on reactivity 




Kaggle:
https://www.kaggle.com/datasets/azathoth42/myanimelist (MyAnimeList Dataset, 2018)
https://www.kaggle.com/code/diminini/vhci-anime (Jupyter Notebook to preprocess dataset)


Images:
https://chatgpt.com/ 


Tap Icon:
https://www.flaticon.com/free-icon/tap_738502 


Angular 17:
https://angular.dev/ 


Angular Material:
https://v17.material.angular.io/ 


Bar Chart:
https://blog.logrocket.com/data-visualization-angular-d3-js/#creating-bar-chart 


Box Plot:
https://d3-graph-gallery.com/boxplot.html 


Radar Chart: 
https://gist.github.com/nbremer/21746a9668ffdf6d8242


Choropleth Map:
https://gist.github.com/wboykinm/dbbe50d1023f90d4e241712395c27fb3#file-us-states-json