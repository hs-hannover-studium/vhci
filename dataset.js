const fs = require('fs');
const moment = require('moment');
const _ = require('lodash');

// Helper function to generate random dates
function randomDate(start, end) {
  let startDate = moment(start);
  let endDate = moment(end);
  let diff = endDate.diff(startDate);
  let randomDiff = _.random(0, diff);
  return startDate.add(randomDiff, 'milliseconds').format();
}

// Anime dataset generation
const animeCount = 20;
const animeData = {
  id: _.range(1, animeCount + 1),
  name: _.range(1, animeCount + 1).map(i => `Anime-${i}`),
  episodes: _.times(animeCount, () => _.sample([12, 24, 25])),
  airedDate: _.times(animeCount, () => randomDate('2000-01-01', '2020-01-01')),
  genre: _.times(animeCount, () => _.sample(["Action", "Romance", "Sci-Fi", "Adventure", "Fantasy", "Comedy"])),
};

// Food dataset generation
const foodData = {
  id: [1, 2, 3],
  name: ["Spaghetti", "Yumyum", "Delivery"],
};


// State dataset generation
const statesOfGermany = [
  ["Baden-Württemberg", 11069533], ["Bavaria", 13076721], ["Berlin", 3644826],
  ["Brandenburg", 2511917], ["Bremen", 682986], ["Hamburg", 1841179],
  ["Hesse", 6265809], ["Lower Saxony", 7982448], ["Mecklenburg-Vorpommern", 1609675],
  ["North Rhine-Westphalia", 17932651], ["Rhineland-Palatinate", 4084844],
  ["Saarland", 990509], ["Saxony", 4077937], ["Saxony-Anhalt", 2208321],
  ["Schleswig-Holstein", 2896712], ["Thuringia", 2143145]
];

// States of united states
const statesOfUSA = [
   "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
    "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
    "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan",
    "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire",
  ]
const stateData = {
  id: _.range(1, statesOfUSA.length),
  name: statesOfUSA,
};

// User dataset generation
const userCount = 200;
const userData = {
  id: _.range(1, userCount + 1),
  username: _.range(1, userCount + 1).map(i => `user${i}`),
  gender: _.times(userCount, () => _.sample(["male", "female", "other"])),
  age: _.times(userCount, () => _.random(10, 45)),
  // countryId: _.times(userCount, () => _.sample(_.range(1, statesOfUSA.length + 1))),
  country: _.times(userCount, () => _.sample(statesOfUSA)),
};

// Watch session dataset generation
const watchSessionCount = 10;
let watchSessionData = {
  userId: _.times(watchSessionCount, () => _.sample(_.range(1, userCount + 1))),
  animeId: _.times(watchSessionCount, () => _.sample(_.range(1, animeCount + 1))),
  foodId: _.times(watchSessionCount, () => _.sample([1, 2, 3])),
  startTime: _.times(watchSessionCount, () => randomDate(moment().subtract(4, 'years').format(), moment().format())),
  endTime: [],
};

// Generate end time based on start time
const maxWatchDurationHours = 3;
watchSessionData.startTime.forEach((start, index) => {
  let endTime = moment(start).add(_.random(1, maxWatchDurationHours), 'hours').add(_.random(0, 59), 'minutes').format();
  watchSessionData.endTime.push(endTime);
});

// Saving to CSV
function saveToCSV(filename, data) {
  const headers = Object.keys(data);
  const csvContent = data[headers[0]].map((_, index) => {
    return headers.map(header => data[header][index]).join(',');
  }).join('\n');
  fs.writeFileSync(filename, headers.join(',') + '\n' + csvContent, 'utf8');
}

const dataDir = 'src/assets/data';

saveToCSV(dataDir + '/anime.csv', animeData);
saveToCSV(dataDir + '/food.csv', foodData);
saveToCSV(dataDir + '/user.csv', userData);
saveToCSV(dataDir + '/state.csv', stateData);
saveToCSV(dataDir + '/watch_session.csv', watchSessionData);
