import {AsyncPipe, JsonPipe, NgForOf, NgIf} from "@angular/common";
import {Component, inject, OnInit} from '@angular/core';
import {toObservable} from "@angular/core/rxjs-interop";
import {FormsModule} from "@angular/forms";
import {MatCard, MatCardContent, MatCardHeader, MatCardTitle} from "@angular/material/card";
import {MatFormField, MatLabel, MatOption, MatSelect} from "@angular/material/select";
import {RouterOutlet} from '@angular/router';
import {getState} from "@ngrx/signals";

import {AppStore, DataSet} from "./app.store";
import {
  AverageAgeByGenderChartComponent
} from "./components/average-age-by-gender-chart/average-age-by-gender-chart.component";
import {BarChartComponent} from "./components/base-charts/bar-chart/bar-chart.component";
import {MapChartComponent} from "./components/base-charts/map-chart/map-chart.component";
import {ChartHeadingComponent} from "./components/chart-heading/chart-heading.component";
import {
  FavoriteGenreByGenderChartComponent
} from "./components/favorite-genre-by-gender-chart/favorite-genre-by-gender-chart.component";
import {TomoChanComponent} from "./components/tomo-chan/tomo-chan.component";
import {UserGenderCountChartComponent} from "./components/user-gender-count-chart/user-gender-count-chart.component";
import {
  UserLocationCountChartComponent
} from "./components/user-location-count-chart/user-location-count-chart.component";
import {registeredThemes, Theme} from "./models/Themes/theme";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, BarChartComponent,
    AsyncPipe, NgIf, AverageAgeByGenderChartComponent,
    MapChartComponent,
    UserLocationCountChartComponent, UserGenderCountChartComponent,
    JsonPipe, TomoChanComponent, MatCard, MatCardHeader, MatCardContent, MatCardTitle, FavoriteGenreByGenderChartComponent, ChartHeadingComponent, FormsModule, MatSelect, MatOption, NgForOf, MatFormField, MatLabel],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit {
  public store = inject(AppStore);

  public title = "WHO'S YOUR TYPICAL ANIME WATCHER?";
  public theme$ = toObservable(this.store.activeTheme);
  public isLoading$ = toObservable(this.store.isLoading);
  public selectedData!: DataSet;
  public selectedTheme!: Theme;

  public dataSets: DataSet[] = ["MyAnimeList 2018", "Test"];
  public themes: Theme[] = Object.values(registeredThemes);

  ngOnInit() {
    this.isLoading$.pipe().subscribe(isLoading => {
      this.selectedData = getState(this.store).activeDataSet;
      const activeTheme = getState(this.store).activeTheme;
      this.selectedTheme = this.themes.find(theme => theme.name === activeTheme.name) || getState(this.store).activeTheme;
    });
  }

  onDataChange() {
    this.store.loadData(this.selectedData);
  }

  onThemeChange() {
    this.store.setTheme(this.selectedTheme);
  }
}
