import {filter, pipe} from "rxjs";

function isNotUndefined<T>(value: T | undefined): value is T {
  return value !== undefined;
}

function isNotNullOrUndefined<T>(value: T | null | undefined): value is T {
  return value !== null && value !== undefined;
}

// Custom operator to filter out null or undefined values
export const filterNullOrUndefined = () => pipe(
  filter(isNotNullOrUndefined)
);

export const filterUndefined = () => pipe(
  filter(isNotUndefined)
);
