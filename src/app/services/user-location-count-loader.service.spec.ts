import { TestBed } from '@angular/core/testing';

import { UserLocationCountLoaderService } from './user-location-count-loader.service';

describe('UserLocationCountLoaderService', () => {
  let service: UserLocationCountLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserLocationCountLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
