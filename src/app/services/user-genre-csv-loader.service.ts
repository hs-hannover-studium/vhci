import { Injectable } from '@angular/core';
import { map } from "rxjs/operators";

import { UserGenre } from '../models/user-genre';
import { CsvLoader } from "./csv.loader";
import { CsvLoaderService } from "./csv-loader.service";

@Injectable({
  providedIn: 'root'
})
export class UserGenreCsvLoaderService implements CsvLoader<UserGenre> {

  constructor(private readonly csvLoaderService: CsvLoaderService) {
  }

  public loadMappedCsvData(filePath: string) {
    return this.csvLoaderService.loadCsvData(filePath).pipe(
      map(this.mapCsvDataToList));
  }

  public mapCsvDataToList(data: string): UserGenre[] {
    const lines = data.split("\n");

    lines.shift();

    return lines.map((line: string) => {
      const [id, name, country, gender, genre] = line.split("|");
      const genres = genre ? genre.split(",").map(genre => genre.trim()) : [];

      return { id, name, country, gender, genres: genres };
    });
  }
}
