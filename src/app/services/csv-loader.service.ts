import {HttpClient} from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CsvLoaderService {

  constructor(private http: HttpClient) { }

  public loadCsvData(filePath: string) {
    return this.http.get(filePath, { responseType: 'text' });
  }
}
