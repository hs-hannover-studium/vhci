import { TestBed } from '@angular/core/testing';

import { WatchsessionCsvLoaderService } from './watchsession-csv-loader.service';

describe('WatchsessionCsvLoaderService', () => {
  let service: WatchsessionCsvLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WatchsessionCsvLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
