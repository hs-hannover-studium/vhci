import {Observable} from "rxjs";

export interface CsvLoader<T> {
  loadMappedCsvData(filePath: string): Observable<T[]>;

  mapCsvDataToList(data: string): T[];
}
