import {Injectable} from '@angular/core';
import {map} from "rxjs";

import {Watchsession} from "../models/watchsession";
import {CsvLoader} from "./csv.loader";
import {CsvLoaderService} from "./csv-loader.service";

@Injectable({
  providedIn: 'root'
})
export class WatchsessionCsvLoaderService implements CsvLoader<Watchsession> {

  constructor(private readonly csvLoaderService: CsvLoaderService) {
  }

  loadMappedCsvData(filePath: string) {
    return this.csvLoaderService.loadCsvData(filePath).pipe(
      map(this.mapCsvDataToList));
  }

  mapCsvDataToList(data: string): Watchsession[] {
    const lines = data.split("\n");

    lines.shift();

    return lines.map((line: string) => {
      const [userId, animeId, foodId, startTime, endTime] = line.split(",");

      return {userId: userId, animeId: animeId, foodId: foodId, startTime, endTime};
    });
  }
}
