import {Injectable} from '@angular/core';
import {map, Observable} from "rxjs";

import {UserLocationCount} from "../models/user-location.count";
import {CsvLoader} from "./csv.loader";
import {CsvLoaderService} from "./csv-loader.service";

@Injectable({
  providedIn: 'root'
})
export class UserLocationCountLoaderService implements CsvLoader<UserLocationCount> {

  constructor(private readonly csvLoaderService: CsvLoaderService) {
  }

  public loadMappedCsvData(filePath: string): Observable<UserLocationCount[]> {
    return this.csvLoaderService.loadCsvData(filePath).pipe(
      map(this.mapCsvDataToList));
  }

  public mapCsvDataToList(data: string): UserLocationCount[] {
    const lines = data.split("\n");

    lines.shift();
    lines.pop();

    return lines.map((line: string) => {
      const [state, count] = line.split(",");

      return {state, count: parseInt(count)};
    });
  }
}
