import { TestBed } from '@angular/core/testing';

import { UserGenreCsvLoaderService } from './user-genre-csv-loader.service';

describe('UserGenreCsvLoaderService', () => {
  let service: UserGenreCsvLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserGenreCsvLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
