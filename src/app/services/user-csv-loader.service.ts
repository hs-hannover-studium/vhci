import { Injectable } from '@angular/core';
import { map } from "rxjs";

import { activeGenres, allGenres } from '../models/animeGenres';
import { User } from "../models/user";
import { CsvLoader } from "./csv.loader";
import { CsvLoaderService } from "./csv-loader.service";
import { GenderType } from '../models/genderType';

@Injectable({
  providedIn: 'root'
})
export class UserCsvLoaderService implements CsvLoader<User> {
  constructor(private readonly csvLoaderService: CsvLoaderService) {
  }

  public loadMappedCsvData(filePath: string) {
    return this.csvLoaderService.loadCsvData(filePath).pipe(
      map(this.mapCsvDataToList)
    )
  }

  public mapCsvDataToList(data: string): User[] {
    const lines = data.split("\n");

    lines.shift();

    return lines.map((line: string) => {
      const columns = line.split(",");
      const user: User = {
        id: columns[0],
        name: columns[1],
        country: columns[2],
        gender: (columns[3] as GenderType),
        age: parseInt(columns[5]),
        genres: new Map<string, number>()
      };

      for (let i = 0; i < allGenres.length; i++) {
        if (activeGenres.includes(allGenres[i])) {
          user.genres.set(allGenres[i], parseInt(columns[i + 6]));
        }
      }

      return user;
    });
  }
}
