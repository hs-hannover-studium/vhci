import { TestBed } from '@angular/core/testing';

import { UserCsvLoaderService } from './user-csv-loader.service';

describe('UserCsvLoaderService', () => {
  let service: UserCsvLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserCsvLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
