import {DestroyRef, inject} from "@angular/core";
import {takeUntilDestroyed} from "@angular/core/rxjs-interop";
import {getState, patchState, signalStore, withHooks, withMethods, withState} from '@ngrx/signals';
import {combineLatest, delay} from "rxjs";

import {registeredThemes, Theme} from "./models/Themes/theme";
import {User} from "./models/user";
import {UserCsvLoaderService} from "./services/user-csv-loader.service";
import {MatDialog} from "@angular/material/dialog";
import {DialogData, InfoDialogComponent} from "./components/info-dialog/info-dialog.component";
import {ChartDescription} from "./models/chartDescription";

export type DataSet = "MyAnimeList 2018" | "Test"

interface AppStore {
  users: User[];
  activeDataSet: DataSet;
  activeTheme: Theme;
  isLoading: boolean;
}

const initialState: AppStore = {
  users: [],
  activeDataSet: getInitialDataSet(),
  activeTheme: getInitialTheme(),
  isLoading: true,
};

export const AppStore = signalStore(
  {providedIn: 'root'},
  withState(initialState),
  withMethods(store => {
      const userService: UserCsvLoaderService = inject(UserCsvLoaderService);
      const destroyRef = inject(DestroyRef);
      const dialog = inject(MatDialog);

      return {
        setTheme(theme: Theme): void {
          localStorage.setItem('theme', theme.name);

          patchState(store, {
            ...getState(store),
            activeTheme: theme
          });
        },
        openDialog(description: ChartDescription) {
          return dialog.open<InfoDialogComponent, DialogData>(InfoDialogComponent, {
            data: {
              description: description
            },
            maxWidth: '500px'
          });
        },
        loadData(dataSet: DataSet = initialState.activeDataSet): void {
          patchState(store, {
            ...getState(store),
            isLoading: true
          });

          const dataDir = `assets/data/${dataSet}`;
          const users$ = userService.loadMappedCsvData(`${dataDir}/users.csv`);
          localStorage.setItem('dataSet', dataSet);

          combineLatest([users$])
            .pipe(
              delay(1500),
              takeUntilDestroyed(destroyRef)
            )
            .subscribe(([users]) => {
              patchState(store, {
                ...getState(store),
                users: users,
                isLoading: false
              });
            });
        },
      };
    },
  ),
  withHooks({
    onInit: function (store) {
      const description: ChartDescription = {
        title: 'Our Project :)!',
        description: '<p>With this dashboard you can analyze data related to anime and its consumption in the united states.</p> ' +
          '<p>There are different types of interactions for each chart. If you are not sure what you can do, please check out the information that each chart provides with the information button.</p>' +
          '<p>In the bottom right is tomo-chan our kind assisant who summerizes each chart for you.</p>' +
          '<p>We hope you enjoy it!</p>',
      }

      const info = localStorage.getItem('hasInformationViewed');

      if (info) {
        store.loadData();
      }
      if (!info) {
        store.openDialog(description)
          .afterClosed()
          .pipe(takeUntilDestroyed(inject(DestroyRef)))
          .subscribe(() => {
            store.loadData();
            localStorage.setItem('hasInformationViewed', JSON.stringify(true));
          });
      }
    },
    onDestroy(store) {
      console.log('count on destroy', store.isLoading());
    },
  }),
);

function getInitialTheme(): Theme {
  const themes: Theme[] = Object.values(registeredThemes);
  const fallbackTheme = registeredThemes.lollipop;
  const themeNameInLocalStorage = localStorage.getItem('theme');

  return themes.find(theme => theme.name === themeNameInLocalStorage) || fallbackTheme;
}

function getInitialDataSet(): DataSet {
  const validDataSets: DataSet[] = ["MyAnimeList 2018", "Test"];
  const fallbackDataSet: DataSet = 'MyAnimeList 2018';
  let dataSetFromLocalStorage = localStorage.getItem('dataSet') as DataSet;

  if (!validDataSets.includes(dataSetFromLocalStorage)) {
    dataSetFromLocalStorage = fallbackDataSet;
  }

  return dataSetFromLocalStorage;
}
