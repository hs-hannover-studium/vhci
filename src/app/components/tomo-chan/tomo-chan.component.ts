import {AsyncPipe, NgIf} from "@angular/common";
import {Component, inject, OnDestroy, OnInit} from '@angular/core';
import {toObservable} from "@angular/core/rxjs-interop";
import {Subject, takeUntil} from "rxjs";

import {AppStore} from "../../app.store";
import {AverageAgeByGenderStore} from "../average-age-by-gender-chart/average-age-by-gender.store";
import {FavoriteGenreStore} from "../favorite-genre-by-gender-chart/favorite-genre.store";
import {GenderStore as GenderStore} from "../user-gender-count-chart/user-gender-count.store";
import {SelectedStateStore} from "../user-location-count-chart/selected-state.store";

@Component({
  selector: 'app-tomo-chan',
  standalone: true,
  imports: [
    NgIf,
    AsyncPipe
  ],
  templateUrl: './tomo-chan.component.html',
  styleUrl: './tomo-chan.component.scss'
})
export class TomoChanComponent implements OnInit, OnDestroy {
  public selectedStateStore = inject(SelectedStateStore);
  public genderStore = inject(GenderStore);
  public ageStore = inject(AverageAgeByGenderStore);
  public appStore = inject(AppStore);
  public genreStore = inject(FavoriteGenreStore);

  public theme$ = toObservable(this.appStore.activeTheme);

  public tomoSad = "";
  public tomoHappy = "";

  private destroySubject = new Subject<void>();

  ngOnInit(): void {
    this.theme$.pipe(
      takeUntil(this.destroySubject),
    ).subscribe(theme => {
      this.tomoHappy = `url(${theme.tomoChan.happy})`;
      this.tomoSad = `url(${theme.tomoChan.sad})`;
    });
  }

  ngOnDestroy() {
    this.destroySubject.next();
    this.destroySubject.complete();
  }
}
