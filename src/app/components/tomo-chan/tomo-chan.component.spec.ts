import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TomoChanComponent } from './tomo-chan.component';

describe('TomoChanComponent', () => {
  let component: TomoChanComponent;
  let fixture: ComponentFixture<TomoChanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TomoChanComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TomoChanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
