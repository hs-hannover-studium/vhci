import {NgIf} from "@angular/common";
import {Component, Input} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconButton} from "@angular/material/button";
import {MatDialog} from "@angular/material/dialog";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatIcon} from "@angular/material/icon";
import {MatInput} from "@angular/material/input";
import {MatMenu, MatMenuTrigger} from "@angular/material/menu";

import {ChartDescription} from "../../models/chartDescription";
import {DialogData, InfoDialogComponent} from "../info-dialog/info-dialog.component";

@Component({
  selector: 'app-chart-heading',
  standalone: true,
  imports: [
    MatFormField,
    MatIcon,
    MatIconButton,
    MatInput,
    MatLabel,
    NgIf,
    ReactiveFormsModule,
    MatMenuTrigger,
    MatMenu
  ],
  templateUrl: './chart-heading.component.html',
  styleUrl: './chart-heading.component.scss'
})
export class ChartHeadingComponent {
  @Input() title: string | null = null;
  @Input() disableInfo: boolean = false;
  @Input() description: ChartDescription | null = null;

  constructor(private _dialog: MatDialog) {
  }

  showInfo() {
    this._dialog.open<InfoDialogComponent, DialogData>(InfoDialogComponent, {
      data: {
        description: this.description
      },
      maxWidth: '500px'
    });
  }
}
