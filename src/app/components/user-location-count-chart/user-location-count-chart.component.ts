
import {AsyncPipe, NgIf} from "@angular/common";
import {Component, computed, inject, OnDestroy, Signal} from '@angular/core';
import {toObservable} from "@angular/core/rxjs-interop";
import {getState, patchState} from "@ngrx/signals";
import {Observable, Subject, takeUntil} from "rxjs";

import {AppStore} from "../../app.store";
import {ChartDescription} from "../../models/chartDescription";
import {UserLocationCount} from "../../models/user-location.count";
import {BarChartComponent} from "../base-charts/bar-chart/bar-chart.component";
import {MapChartComponent, MapChartConfig, MapChartData} from "../base-charts/map-chart/map-chart.component";
import {SelectedStateStore} from "./selected-state.store";

import { GenderType } from "../../models/genderType";
import { User } from "../../models/user";
import { GenderStore } from "../user-gender-count-chart/user-gender-count.store";
import { allStates } from "../../models/states";

@Component({
  selector: 'app-user-location-count-chart',
  standalone: true,
  imports: [
    MapChartComponent,
    AsyncPipe,
    BarChartComponent,
    NgIf
  ],
  templateUrl: './user-location-count-chart.component.html',
  styleUrl: './user-location-count-chart.component.scss'
})
export class UserLocationCountChartComponent implements OnDestroy {
  private readonly store = inject(SelectedStateStore);
  public readonly appStore = inject(AppStore);

  protected theme = getState(this.appStore).activeTheme;

  public config: MapChartConfig = {
    margin: 20,
    width: 450,
    title: "Fans by State",
    customColors: {
      hoverFillColor: this.theme.choropleth.hover,
      selectedFillColor: this.theme.choropleth.selected,
    },
    choropleth: {
      steps: 6,
      colorScale: this.theme.choropleth.colorScale
    }
  }

  private destroySubject = new Subject<void>();
  private dataSubject = new Subject<MapChartData>();
  public chartData$ = this.dataSubject.asObservable();

  public description: ChartDescription = {
    title: "Fans by State",
    description: "This chart shows the number of fans in each state. " +
      "<br><br><b>Interactions</b>: " +
      "<ul>" +
      "<li>Hover over a state to see the number of fans in that state</li>" +
      "<li>Click on a state to filter the data for this state</li>" +
      "<li>Search for a state to set a state filter</li>" +
      "<li>Change the steps to increase or decrease the granularity of displayed data</li>" +
      "</ul>" +
      "<br><b>Goal</b>: " +
      "Understand where the anime fans are located."
  };

  private selectedTheme$ = toObservable(this.appStore.activeTheme);

  public users: User[] = [];

  private dataState$: Observable<User[]>;

  private genderFilter: GenderType | null = null;

  private readonly selectedGenderSignal: Signal<GenderType | null>;
  private selectedGender$: Observable<GenderType | null>;

  private readonly genderStore = inject(GenderStore);

  constructor() {
    this.selectedTheme$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((theme) => {
      this.theme = theme;
      this.config = {
        ...this.config,
        customColors: {
          hoverFillColor: this.theme.choropleth.hover,
          selectedFillColor: this.theme.choropleth.selected,
        },
        choropleth: {
          steps: this.config.choropleth?.steps ?? 6,
          colorScale: this.theme.choropleth.colorScale
        }
      }
    });

    this.selectedGenderSignal = computed(() => {
      return this.genderStore.selected() ?? null;
    });

    this.selectedGender$ = toObservable(this.selectedGenderSignal);

    this.selectedGender$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((gender) => {
      this.genderFilter = gender;
      this.createChartData();
    });

    this.dataState$ = toObservable(this.appStore.users);

    this.dataState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((users) => {
      this.users = users;
      this.createChartData();
    })
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  private createChartData() {
    let userList = this.users;

    if (this.genderFilter) {
      userList = this.users.filter((user: User) => {
        return user.gender === this.genderFilter;
      });
    }

    this.dataSubject.next({ values: this.countUsersPerState(userList) });
  }

  public onStateSelected(stateName: string | null) {
    if (stateName === null) {
      patchState(this.store, { selected: null });
    } else {
      patchState(this.store, { selected: { name: stateName } });
    }
  }

  private countUsersPerState(users: User[]): UserLocationCount[] {
    const stateCountMap = users.reduce((acc, user) => {
      if (acc[user.country]) {
        acc[user.country] += 1;
      } else {
        acc[user.country] = 1;
      }
      return acc;
    }, {} as Record<string, number>);

    return allStates.map(state => ({
      state,
      count: stateCountMap[state] || 0
    }));
  }
}
