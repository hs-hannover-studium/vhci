import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLocationCountChartComponent } from './user-location-count-chart.component';

describe('UserLocationCountChartComponent', () => {
  let component: UserLocationCountChartComponent;
  let fixture: ComponentFixture<UserLocationCountChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserLocationCountChartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UserLocationCountChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
