import {signalStore, withState} from '@ngrx/signals';

interface SelectedState {
  name: string;
}

type SelectedStateState = {
  selected: SelectedState | null;
};

const initialState: SelectedStateState = {
  selected: null,
};

export const SelectedStateStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
);
