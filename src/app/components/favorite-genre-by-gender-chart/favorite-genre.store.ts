import {signalStore, withState} from '@ngrx/signals';

interface FavoriteGenre {
  genre: string;
}

type FavoriteGenreState = {
  selected: FavoriteGenre | null;
};

const initialState: FavoriteGenreState = {
  selected: null,
};

export const FavoriteGenreStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
);
