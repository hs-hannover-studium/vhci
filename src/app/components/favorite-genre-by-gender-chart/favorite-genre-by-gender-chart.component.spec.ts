import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteGenreByGenderChartComponent } from './favorite-genre-by-gender-chart.component';

describe('FavoriteGenreByGenderChartComponent', () => {
  let component: FavoriteGenreByGenderChartComponent;
  let fixture: ComponentFixture<FavoriteGenreByGenderChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FavoriteGenreByGenderChartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FavoriteGenreByGenderChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
