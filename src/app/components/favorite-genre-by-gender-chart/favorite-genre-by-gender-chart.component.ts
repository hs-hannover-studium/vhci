import {AsyncPipe, NgIf} from '@angular/common';
import {Component, computed, inject, OnDestroy, Signal} from '@angular/core';
import {toObservable} from '@angular/core/rxjs-interop';
import {getState, patchState} from '@ngrx/signals';
import * as d3 from 'd3';
import {Observable, Subject, takeUntil} from 'rxjs';

import {AppStore} from '../../app.store';
import {activeGenres} from '../../models/animeGenres';
import {ChartDescription} from "../../models/chartDescription";
import {GenderType} from "../../models/genderType";
import {Theme} from "../../models/Themes/theme";
import {User} from '../../models/user';
import {
  RadarAxisValue as RadarAxisValue,
  RadarChartComponent,
  RadarChartConfig,
  RadarChartData,
  RadarChartEntry
} from "../base-charts/radar-chart/radar-chart.component";
import {GenderStore} from '../user-gender-count-chart/user-gender-count.store';
import {SelectedStateStore} from '../user-location-count-chart/selected-state.store';
import {FavoriteGenreStore} from './favorite-genre.store';

@Component({
  selector: 'app-favorite-genre-by-gender-chart',
  standalone: true,
  templateUrl: './favorite-genre-by-gender-chart.component.html',
  styleUrl: './favorite-genre-by-gender-chart.component.scss',
  imports: [RadarChartComponent, NgIf, AsyncPipe]
})
export class FavoriteGenreByGenderChartComponent implements OnDestroy {
  public readonly store = inject(AppStore);
  private readonly favoriteGenreStore = inject(FavoriteGenreStore);
  private readonly selectedStateStore = inject(SelectedStateStore);
  private readonly genderStore = inject(GenderStore)

  private theme = getState(this.store).activeTheme;

  public chartConfig: RadarChartConfig = {
    margin: 75,
    width: 300,
    height: 250,
    title: "Favorite Genres",
    levels: 5,
    roundStrokes: true,
    strokeWidth: 1,
    circleOpacity: 0.1,
    areaOpacity: 0.15,
    dotRadius: 4
  }

  public description: ChartDescription = {
    title: "Top Genres",
    description: "This chart displays the top genres most watched by anime fans, based on the filters you have applied (state and gender). " +
      "When you change the state or gender filter, the chart updates to show the top genres for the selected filters." +
      "<br><br><b>Interactions</b>: " +
      "<ul>" +
      "<li>Add or remove additional genres</li>" +
      "<li>Lock genres to retain them when filters are changed</li>" +
      "</ul>" +
      "<br><b>Goal</b>: " +
      "Gain insights into the favorite genres of anime fans."
  }

  private dataState$: Observable<User[]>;

  private stateFilter: string | null = null;

  private readonly selectedStateSignal: Signal<string | null>;
  private selectedState$: Observable<string | null>

  private genderFilter: GenderType | null = null;

  private readonly selectedGenderSignal: Signal<GenderType | null>;
  private selectedGender$: Observable<GenderType | null>;

  private destroySubject = new Subject<void>();
  private dataSubject = new Subject<RadarChartData>();
  private users: User[] = [];

  public chartData$ = this.dataSubject.asObservable();

  private selectedTheme$: Observable<Theme>;

  private genderColors: Record<GenderType, string> = {
    'Male': this.theme.genders.male,
    'Female': this.theme.genders.female,
    'Non-Binary': this.theme.genders.nonBinary
  };

  constructor() {
    // To filter by state
    this.selectedStateSignal = computed(() => {
      return this.selectedStateStore.selected()?.name ?? null;
    });

    this.selectedState$ = toObservable(this.selectedStateSignal);

    this.selectedState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((state) => {
      this.stateFilter = state;
      this.createChartData();
    });

    this.dataState$ = toObservable(this.store.users);

    // To filter by gender
    this.selectedGenderSignal = computed(() => {
      return this.genderStore.selected() ?? null;
    });

    this.selectedGender$ = toObservable(this.selectedGenderSignal);

    this.selectedGender$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((gender) => {
      this.genderFilter = gender;
      this.createChartData();
    });

    this.dataState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((users) => {
      this.users = users;
      this.createChartData();
    })

    this.selectedTheme$ = toObservable(this.store.activeTheme);
    this.selectedTheme$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((theme) => {
      this.theme = theme;
      this.genderColors = {
        'Male': this.theme.genders.male,
        'Female': this.theme.genders.female,
        'Non-Binary': this.theme.genders.nonBinary
      };
      this.createChartData();
    });
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  private createChartData() {
    let users = this.users;

    if (this.stateFilter) {
      users = users.filter((user: User) => {
        return user.country === this.stateFilter;
      });
    }

    if (this.genderFilter) {
      users = users.filter((user: User) => {
        return user.gender === this.genderFilter;
      });
    }

    const {genrePercentagesByGender} = this.calculateGenrePercentages(users);

    const radarChartData: RadarChartData = {
      values: Array.from(genrePercentagesByGender.entries()).map(([gender, axisValues]) => ({
        className: gender,
        axisValues,
        color: this.genderColors[gender]
      }))
    };

    this.dataSubject.next(radarChartData);
  }


  private calculateGenrePercentages(users: User[]): {
    genrePercentagesByGender: Map<GenderType, RadarAxisValue[]>,
    totalPercentages: RadarAxisValue[]
  } {
    const genreCountsByGender = new Map<GenderType, RadarAxisValue[]>();
    const totalCountByGender = new Map<GenderType, number>();
    const genders = (['Male', 'Female', 'Non-Binary'] as GenderType[]);

    genders.forEach(gender => {
      genreCountsByGender.set(gender, activeGenres.map(genre => ({axis: genre, value: 0})));
      totalCountByGender.set(gender, 0);
    });

    users.forEach(user => {
      const genderGenres = genreCountsByGender.get(user.gender);
      user.genres.forEach((count, genre) => {
        const genreData = genderGenres?.find(g => g.axis === genre);
        if (genreData) {
          genreData.value += count;
        }
        totalCountByGender.set(user.gender, totalCountByGender.get(user.gender)! + count);
      });
    });

    totalCountByGender.forEach((value, key) => {
      if (value === 0) {
        genreCountsByGender.delete(key)
      }
    });

    const genrePercentagesByGender = new Map<GenderType, RadarAxisValue[]>();
    genreCountsByGender.forEach((counts, gender) => {
      const totalGenderCount = totalCountByGender.get(gender) || 1;
      const percentages = counts.map(({axis, value}) => {
        return {
          axis: axis,
          value: value / totalGenderCount
        }
      });
      genrePercentagesByGender.set(gender, percentages);
    });

    const totalPercentages = activeGenres.map(genre => {
      const totalGenderCount = Array.from(totalCountByGender.values()).reduce((sum, g) => sum + g, 0);
      const totalGenreCount = Array.from(genreCountsByGender.values()).reduce((sum, g) => sum + (g.find(g => g.axis === genre)?.value || 0), 0);
      return {
        axis: genre,
        value: totalGenderCount > 0 ? totalGenreCount / totalGenderCount : 0
      };
    });

    const result = {
      genrePercentagesByGender,
      totalPercentages
    };

    return result;
  }

  onDataChanged(entries: RadarChartEntry[]) {
    const gender = this.genderStore.selected() ?? this.genderStore.dominant();

    const genderEntry = entries.map(x => Object.assign({}, x))
      .find(entry => {
        return entry.className.toLowerCase() === gender?.toLowerCase();
      })?.axisValues.map(x => {
        return Object.assign({}, x);
      }).sort((a, b) => b.value - a.value);

    if (!genderEntry || (genderEntry && genderEntry.length === 0)) {
      return;
    }

    const favGenre = genderEntry[0].axis;

    patchState(this.favoriteGenreStore, {
      selected: {
        genre: favGenre,
      }
    });
  }
}
