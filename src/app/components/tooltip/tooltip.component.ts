import { NgIf } from '@angular/common';
import { AfterViewInit, Component, ElementRef, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  standalone: true,
  imports: [NgIf],
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class TooltipComponent implements AfterViewInit, OnChanges {
  @Input() content: { title: string; value?: string | null; value2?: string | null } = { title: '', value: null, value2: null };
  @Input() position: { x: number; y: number } = { x: 0, y: 0 };

  constructor(private el: ElementRef) {}

  ngAfterViewInit(): void {
    this.adjustPosition();
  }

  ngOnChanges(): void {
    this.adjustPosition();
  }

  private adjustPosition() {
    const tooltipElement = this.el.nativeElement.querySelector('.tooltip');
    if (tooltipElement) {
      const tooltipHeight = tooltipElement.offsetHeight;

      const offsetX = 5;
      const offsetY = 5;

      tooltipElement.style.left = `${this.position.x + offsetX}px`;
      tooltipElement.style.top = `${this.position.y - tooltipHeight - offsetY}px`;
    }
  }
}
