import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGenderCountChartComponent } from './user-gender-count-chart.component';

describe('UserGenderCountChartComponent', () => {
  let component: UserGenderCountChartComponent;
  let fixture: ComponentFixture<UserGenderCountChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UserGenderCountChartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UserGenderCountChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
