import { signalStore, withState } from '@ngrx/signals';

import { GenderType } from "../../models/genderType";

type GenderState = {
  dominant: GenderType | null;
  selected: GenderType | null;
};

const initialState: GenderState = {
  dominant: null,
  selected: null
};

export const GenderStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
);
