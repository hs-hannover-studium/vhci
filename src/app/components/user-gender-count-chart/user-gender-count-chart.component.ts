import {AsyncPipe, NgIf} from "@angular/common";
import {Component, computed, inject, OnDestroy, Signal} from '@angular/core';
import {toObservable} from "@angular/core/rxjs-interop";
import {getState, patchState} from "@ngrx/signals";
import {Observable, Subject, takeUntil} from "rxjs";

import {AppStore} from "../../app.store";
import {ChartDescription} from "../../models/chartDescription";
import {GenderType} from "../../models/genderType";
import {Theme} from "../../models/Themes/theme";
import {User} from "../../models/user";
import {
  BarChartComponent,
  BarChartConfig,
  BarChartData,
  BarChartEntry
} from "../base-charts/bar-chart/bar-chart.component";
import {SelectedStateStore} from "../user-location-count-chart/selected-state.store";
import {GenderStore} from "./user-gender-count.store";

@Component({
  selector: 'app-user-gender-count-chart',
  standalone: true,
  imports: [
    BarChartComponent,
    AsyncPipe,
    NgIf
  ],
  templateUrl: './user-gender-count-chart.component.html',
  styleUrl: './user-gender-count-chart.component.scss'
})
export class UserGenderCountChartComponent implements OnDestroy {
  private readonly selectedStateStore = inject(SelectedStateStore);
  private readonly selectedGenderCountStore = inject(GenderStore);
  private readonly store = inject(AppStore);

  public barChartConfig: BarChartConfig = {
    margin: 50,
    width: 475,
    height: 190,
    title: "Gender count",
    showLegend: false,
    useCustomColors: true,
    domain: {min: 0, maxOffset: 20},
    showAverage: false,
  }

  protected theme = getState(this.store).activeTheme;

  private stateFilter: string | null = null;

  private destroySubject = new Subject<void>();
  private barChartDataSubject = new Subject<BarChartData>();
  public users: User[] = [];
  public description: ChartDescription = {
    title: "Gender count",
    description: "This chart shows the gender distribution of your selected state or the whole dataset and acts as a gender filter for other charts." +
      "<br><br><b>Interactions</b>: " +
      "<ul>" +
      "<li>Click on a gender image to select or deselect a gender filter.</li>" +
      "</ul>" +
      "<br><b>Goal</b>: " +
      "Understand the gender distribution of anime fans."
  }

  private dataState$: Observable<User[]>;
  private selectedTheme$: Observable<Theme>;

  private readonly selectedStateSignal: Signal<string | null>;
  private selectedState$: Observable<string | null>

  public barChartData$ = this.barChartDataSubject.asObservable();

  constructor() {
    this.selectedStateSignal = computed(() => {
      return this.selectedStateStore.selected()?.name ?? null;
    });

    this.selectedState$ = toObservable(this.selectedStateSignal);

    this.selectedState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((state) => {
      this.stateFilter = state;
      this.createChartData();
    })

    this.dataState$ = toObservable(this.store.users);

    this.dataState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((users) => {
      this.users = users;
      this.createChartData();
    });

    this.selectedTheme$ = toObservable(this.store.activeTheme);

    this.selectedTheme$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((theme) => {
      this.theme = theme;
      this.createChartData();
    });
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  private createChartData() {
    let userList = this.users;

    if (this.stateFilter) {
      userList = this.users.filter((user: User) => {
        return user.country === this.stateFilter;
      });
    }

    const maleCount = this.calculateCountByGender(userList, "Male", this.theme.genders.male, this.theme.barChart.male);
    const femaleCount = this.calculateCountByGender(userList, "Female", this.theme.genders.female, this.theme.barChart.female);
    const otherCount = this.calculateCountByGender(userList, "Non-Binary", this.theme.genders.nonBinary, this.theme.barChart.nonBinary);

    const data: BarChartEntry[] = [femaleCount, maleCount, otherCount];

    const dominantGender = [...data].sort((a, b) => b.y - a.y)[0];

    patchState(this.selectedGenderCountStore, {dominant: dominantGender.id as GenderType});

    this.barChartDataSubject.next({values: data});
  }

  private calculateCountByGender(users: User[], gender: GenderType, color: string, image?: string): BarChartEntry {
    const count = users.filter(a => a.gender === gender).length;

    return {id: gender, x: gender, y: count, color: color, image: image};
  }

  public onGenderSelected(gender: string | null) {
    if (gender === null) {
      patchState(this.selectedGenderCountStore, {selected: null});
    } else {
      patchState(this.selectedGenderCountStore, {selected: (gender as GenderType)}); }
  }
}
