import {NgForOf, NgIf} from "@angular/common";
import {AfterViewInit, Component, inject, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconButton} from "@angular/material/button";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatIcon} from "@angular/material/icon";
import {MatInput} from "@angular/material/input";
import {MatMenu, MatMenuItem, MatMenuTrigger} from "@angular/material/menu";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {patchState} from "@ngrx/signals";
import * as d3 from "d3";
import {ScaleBand, ScaleLinear} from "d3";

import {ChartConfig} from "../../../models/chart.config";
import {ChartDescription} from "../../../models/chartDescription";
import {AverageAgeByGenderStore} from "../../average-age-by-gender-chart/average-age-by-gender.store";
import {ChartHeadingComponent} from "../../chart-heading/chart-heading.component";
import { TooltipComponent } from "../../tooltip/tooltip.component";
import {GenderStore} from "../../user-gender-count-chart/user-gender-count.store";
import {Chart} from "../bar-chart/bar-chart.component";

export interface BoxPlotConfig extends ChartConfig {
  height: number;
  domain: { min: number, max?: number, maxOffset: number };
}

export interface BoxPlotData {
  values: BoxPlotEntry[];
}

export interface BoxPlotEntry {
  id: string;
  x: string;
  y: number;
  color: string;
}

interface Entry {
  key: string
  color: string
  value: {
    q1: number
    median: number
    q3: number
    interQuantileRange: number
    min: number
    max: number
  }
}

@Component({
  selector: 'app-box-plot-chart',
  standalone: true,
  imports: [
    MatFormField,
    MatIcon,
    MatIconButton,
    MatInput,
    MatLabel,
    MatMenu,
    MatMenuItem,
    MatSlideToggle,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    MatMenuTrigger,
    ChartHeadingComponent,
    TooltipComponent
  ],
  templateUrl: './box-plot-chart.component.html',
  styleUrl: './box-plot-chart.component.scss'
})
export class BoxPlotChartComponent extends Chart implements AfterViewInit, OnChanges {
  @Input() config!: BoxPlotConfig;
  @Input() data!: BoxPlotData
  @Input() description: ChartDescription = { title: '', description: '' };

  private boxWidth = 25;
  private transitionDuration = 750;

  readonly store = inject(AverageAgeByGenderStore);
  readonly genderStore = inject(GenderStore)

  public tooltipContent = { title: '', value: '', value2: '' };
  public tooltipPosition = { x: 0, y: 0 };
  public tooltipVisible = false;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data'] && !changes['data'].firstChange) {
      this.update();
    }
  }

  ngAfterViewInit(): void {
    this.createSvg()
    this.update();
  }

  private createSvg() {
    const chart = d3.select(`svg#${this.chartId}`)
      .attr("width", this.config.width + this.config.margin)
      .attr("height", this.config.height + (this.config.margin/2))
      .append("g")
      .attr("transform", "translate(" + (this.config.margin - 25) + "," + (this.config.margin - 55) + ")")

    chart.append("g")
      .attr("transform", "translate(0," + this.config.height + ")")
      .attr('class', 'x-axis')

    chart.append("g")
      .attr('class', 'y-axis');

    chart.append("g").attr("class", "vert-line-group");
    chart.append("g").attr("class", "box-group");
    chart.append("g").attr("class", "horizontal-line-group");

    this.svg = chart;
  }

  private update() {
    let xDomain = this.data.values.map(d => d.x);

    if (this.data.values.length === 0) {
      xDomain = ["No Data"];
    }

    const sumstat = this.createBoxPlotData();
    const gender = this.genderStore.selected() ?? this.genderStore.dominant();

    patchState(this.store, {
      selected: {
        median: sumstat.filter(d => d.key === gender)[0]?.value.median ?? null,
      }
    });

    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.config.width])
      .domain(xDomain)
      .paddingInner(1)
      .paddingOuter(.5);

    // Create the Y-axis band scale
    let domainMaxValue;

    if (this.config.domain.max) {
      domainMaxValue = this.config.domain.max + this.config.domain.maxOffset;
    } else {
      domainMaxValue = Math.max(...this.data.values.map(v => v.y)) + this.config.domain.maxOffset;
    }

    const y = d3.scaleLinear()
      .domain([this.config.domain.min, domainMaxValue])
      .range([this.config.height, 0]);

    // Update der Achsen
    this.svg.select(".x-axis")
      .call(d3.axisBottom(x));

    this.svg.select(".y-axis")
      .transition()
      .duration(this.transitionDuration)
      .call(d3.axisLeft(y));

    this.drawVerticalLines(sumstat, x, y);
    this.drawBoxes(sumstat, x, y);
    this.drawHorizontalLines(sumstat, x, y);
  }

  private createBoxPlotData(): Entry[] {
    const dataSortedByX = this.data.values.sort((a, b) => d3.ascending(a.x, b.x));
    const groupedData = d3.group(dataSortedByX, d => d.x);

    return Array.from(groupedData, ([key, values]) => {
      const yValues = values.map(g => g.y);
      const color = values[0].color;
      const q1 = d3.quantile(yValues, .25)!;
      const median = d3.quantile(yValues, .5)!;
      const q3 = d3.quantile(yValues, .75)!;
      const interQuantileRange = q3 - q1;

      // Calculate potential min and max values
      const potentialMin = q1 - 1.5 * interQuantileRange;
      const potentialMax = q3 + 1.5 * interQuantileRange;

      // Constrain min and max values to the actual data range
      const actualMin = d3.min(yValues)!;
      const actualMax = d3.max(yValues)!;

      const min = Math.max(potentialMin, actualMin);
      const max = Math.min(potentialMax, actualMax);

      return {key, color, value: {q1, median, q3, interQuantileRange, min, max}};
    });
  }

  /**
   * Draw the boxes for the box plot
   * @param sumstat
   * @param x
   * @param y
   * @private
   */
  private drawBoxes(sumstat: Entry[], x: ScaleBand<string>, y: ScaleLinear<number, number, never>) {
    const group = this.svg.select(".box-group");
    const selector = "box";

    const entries = group.selectAll("." + selector)
      .data(sumstat, (d: Entry) => d.key + d.color);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("rect")
      .attr("class", selector)
      .attr("x", (d: Entry) => x(d.key)! - this.boxWidth / 2)
      .attr("y", (d: Entry) => y(d.value.q3))
      .attr("height", (d: Entry) => y(d.value.q1) - y(d.value.q3))
      .attr("width", this.boxWidth)
      .attr("median", (d: Entry) => d.value.median)
      .attr("rx", 3)
      .attr("ry", 3)
      .attr("stroke", (d: Entry) => d.color)
      .attr("stroke-width", 1.25+"px")
      .attr("fill-opacity", 0.25)
      .attr("fill", (d: Entry) => d.color)
      .attr("pointer-events", "all")
      .on("mousemove", (event: MouseEvent, d: Entry) => {
        const svgRect = this.svg.node().parentNode.getBoundingClientRect();

        this.tooltipContent = {
          title: "max " + d.value.max,
          value: "med " + d.value.median,
          value2: "min " + d.value.min
        }
        this.tooltipPosition = {
          x: event.clientX - svgRect.left,
          y: event.clientY - svgRect.top
        }
        this.tooltipVisible = true;
      }).on("mouseout", () => {
        this.tooltipVisible = false;
      });

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("x", (d: Entry) => x(d.key)! - this.boxWidth / 2)
      .attr("y", (d: Entry) => y(d.value.q3))
      .attr("median", (d: Entry) => d.value.median)
      .attr("height", (d: Entry) => y(d.value.q1) - y(d.value.q3))
      .attr("width", this.boxWidth);
  }

  /**
   * Show the horizontal lines for median, min, and max
   * @param sumstat
   * @param x
   * @param y
   * @private
   */
  private drawHorizontalLines(sumstat: Entry[], x: ScaleBand<string>, y: ScaleLinear<number, number, never>) {
    const group = this.svg.select(".horizontal-line-group");
    const selector = "horizontalLine"

    type horizontalLineDataType = { key: string, value: number, type: string }

    const horizontalLineData: horizontalLineDataType[] = sumstat.flatMap(d => [
      {key: d.key, value: d.value.median, type: 'median'},
      {key: d.key, value: d.value.min, type: 'min'},
      {key: d.key, value: d.value.max, type: 'max'}
    ]);

    const entries = group.selectAll("." + selector)
      .data(horizontalLineData, (d: horizontalLineDataType) => `${d.key}-${d.type}`);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("line")
      .attr("class", selector)
      .attr("x1", (d: any) => x(d.key)! - this.boxWidth / 2)
      .attr("x2", (d: any) => x(d.key)! + this.boxWidth / 2)
      .attr("y1", (d: any) => y(d.value))
      .attr("y2", (d: any) => y(d.value))
      .attr("stroke", "black")
      .attr("pointer-events", "none");

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("x1", (d: any) => x(d.key)! - this.boxWidth / 2)
      .attr("x2", (d: any) => x(d.key)! + this.boxWidth / 2)
      .attr("y1", (d: any) => y(d.value))
      .attr("y2", (d: any) => y(d.value));
  }

  /**
   * Show the vertical lines for the box plot
   * @param sumstat
   * @param x
   * @param y
   * @private
   */
  private drawVerticalLines(sumstat: Entry[], x: ScaleBand<string>, y: ScaleLinear<number, number, never>) {
    const group = this.svg.select(".vert-line-group");
    const selector = "vertLine";

    const entries = group.selectAll("." + selector)
      .data(sumstat, (d: Entry) => d.key);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("line")
      .attr("class", selector)
      .attr("x1", (d: Entry) => x(d.key)!)
      .attr("x2", (d: Entry) => x(d.key)!)
      .attr("y1", (d: Entry) => y(d.value.min))
      .attr("y2", (d: Entry) => y(d.value.max))
      .attr("stroke", "black")

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("x1", (d: Entry) => x(d.key)!)
      .attr("x2", (d: Entry) => x(d.key)!)
      .attr("y1", (d: Entry) => y(d.value.min))
      .attr("y2", (d: Entry) => y(d.value.max))
  }
}
