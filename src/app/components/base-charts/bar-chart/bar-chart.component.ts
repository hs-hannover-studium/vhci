import {NgForOf, NgIf, NgStyle} from "@angular/common";
import {AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy,OnInit, Output, SimpleChanges, inject} from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatCheckbox} from "@angular/material/checkbox";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatIcon} from "@angular/material/icon";
import {MatInput} from "@angular/material/input";
import {MatMenuItem} from "@angular/material/menu";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import * as d3 from 'd3';
import {ScaleBand, ScaleLinear} from "d3";
import {distinctUntilChanged, Subject, takeUntil} from "rxjs";
import tinycolor from "tinycolor2";

import {ChartConfig} from "../../../models/chart.config";
import {ChartDescription} from "../../../models/chartDescription";
import {ChartHeadingComponent} from "../../chart-heading/chart-heading.component";
import { AppStore } from "../../../app.store";
import { GenderStore } from "../../user-gender-count-chart/user-gender-count.store";

type SortOrder = "ASC" | "DESC" | "NONE";

export interface BarChartConfig extends ChartConfig {
  domain: { min: number, max?: number, maxOffset: number };
  height: number;
  showAverage?: boolean;
  sort?: SortOrder;
  showLegend?: boolean;
  useCustomColors?: boolean;
}

export interface BarChartData {
  values: BarChartEntry[];
}

export interface BarChartEntry {
  id: string;
  x: string;
  y: number;
  color?: string;
  image?: string;
}

export interface BarChartSelectEvent {
  entry: BarChartEntry;
  event: MouseEvent;
}

export interface BarChartLegendData {
  label: string;
  color: string;
}

export class Chart {
  protected svg: any;
  protected chartId = this.generateId();

  protected generateId(length: number = 10): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }
}

@Component({
  selector: 'app-bar-chart',
  standalone: true,
  imports: [
    NgForOf,
    NgStyle,
    NgIf,
    MatButton,
    MatCheckbox,
    MatIconButton,
    MatIcon,
    ChartHeadingComponent,
    MatFormField,
    MatInput,
    MatLabel,
    MatMenuItem,
    MatSlideToggle,
    ReactiveFormsModule
  ],
  templateUrl: './bar-chart.component.html',
  styleUrl: './bar-chart.component.scss'
})
export class BarChartComponent extends Chart implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() config!: BarChartConfig;
  @Input() data!: BarChartData;
  @Input() description: ChartDescription = {title: "", description: ""};

  @Output() selectedGender = new EventEmitter<string | null>();

  private selectedSubject = new Subject<string | null>();
  private readonly destroySubject = new Subject<void>();

  private selected = new Subject<BarChartSelectEvent>();
  public selected$ = this.selected.asObservable();

  public legendData: BarChartLegendData[] = [];

  private defaultBarColor = "#f11653";
  private defaultSelectedColor = "#f44879";
  private transitionDuration = 750;
  private rectWidth = 25;
  private imageOffset = 95;
  
  private appStore = inject(AppStore);
  private genderStore = inject(GenderStore);

  ngOnInit() {
    this.legendData = this.data.values.map((entry: BarChartEntry) => {
      return {
        label: entry.x,
        color: entry.color ?? this.defaultBarColor
      }
    });

    this.selectedSubject
    .pipe(
      distinctUntilChanged(),
      takeUntil(this.destroySubject)
    )
    .subscribe((gender) => {
      this.selectedGender.emit(gender);
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data'] && !changes['data'].firstChange) {
      this.update();
    }
  }

  ngAfterViewInit() {
    this.createSvg();
    this.update();
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  private sort(data: BarChartEntry[], order: SortOrder): BarChartEntry[] {
    if (order === "ASC") {
      return data.sort((a, b) => {
        if (a.y < b.y) return -1;
        if (a.y > b.y) return 1;
        return 0;
      });
    } else {
      return data.sort((a, b) => {
        if (a.y > b.y) return -1;
        if (a.y < b.y) return 1;
        return 0;
      });
    }
  }

  private createSvg(): void {
    const chart = d3.select(`svg#${this.chartId}`)
      .attr("width", this.config.width + this.config.margin)
      .attr("height", this.config.height + this.config.margin)
      .append("g")
      .attr("transform", "translate(" + (this.config.margin - 25) + "," + (this.config.margin - 20) + ")")
      .append("g")
      .attr("class", "chart")

    chart.append("g")
      .attr('class', 'y-axis');

    chart.append("g")
      .attr("transform", "translate(0," + this.config.height + ")")
      .attr('class', 'x-axis')

    chart.append("g")
      .attr("class", "images")

    chart.append("g")
      .attr("class", "values")

    chart.append("g")
      .attr("class", "values-absolute-group")

    this.svg = chart;
  }

  private update(): void {
    if (this.config.sort) {
      this.data.values = this.sort(this.data.values, this.config.sort);
    }

    this.data.values.forEach(v => {
      v.x = v.x.charAt(0).toUpperCase() + v.x.slice(1);
    });

    const chart = this.svg;

    // Create the Y-axis band scale
    const domainMaxValue = this.config.domain.max ?? Math.max(...this.data.values.map(v => v.y)) + this.config.domain.maxOffset;
    const y = d3.scaleLinear()
      .domain([this.config.domain.min, domainMaxValue])
      .range([this.config.height, 0]);

    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.config.width])
      .domain(this.data.values.map(d => d.x))
      .padding(0.2);

    // Update der Achsen
    chart.select(".x-axis")
      .transition()
      .duration(this.transitionDuration)
      .call(d3.axisBottom(x));

    this.drawBars(x, y);
    this.drawValueLabels(x, y);
    this.drawAverageLine(y);
    this.drawImages(x);
  }

  private drawAverageLine(y: d3.ScaleLinear<number, number>): void {
    if (!(this.config.showAverage && this.data.values.length > 0)) {
      return;
    }

    const averageValue = this.data.values.reduce((acc, value) => acc + value.y, 0) / this.data.values.length;
    const padding = 10;
    const averageLine = this.svg.select(".average-line");

    if (!averageLine.empty()) {
      averageLine.transition().duration(this.transitionDuration)
        .attr("x1", padding)
        .attr("x2", this.config.width - padding)
        .attr("y1", y(averageValue))
        .attr("y2", y(averageValue));
    } else {
      this.svg.append("line")
        .attr("class", "average-line")
        .attr("x1", padding)
        .attr("x2", this.config.width - padding)
        .attr("y1", y(averageValue))
        .attr("y2", y(averageValue))
        .style("stroke", "black")
        .style("stroke-width", "3px")
        .style("stroke-dasharray", "4");
    }
  }

  private onMouseOut(event: MouseEvent, entry: BarChartEntry) {
    d3.select(`#${this.chartId}` + " ." + entry.id)
      .attr("fill", this.config.useCustomColors ? (entry.color ?? this.defaultBarColor) : this.defaultBarColor);
  }

  private onMouseOver(event: MouseEvent, entry: BarChartEntry): void {
    const selectedColor = tinycolor(this.config.useCustomColors ? (entry.color ?? this.defaultSelectedColor) : this.defaultSelectedColor).lighten(10).toString();

    d3.select(`#${this.chartId}` + " ." + entry.id)
      .attr("fill", selectedColor);

    this.selected.next({entry: entry, event: event});
  }

  private drawImages(x: ScaleBand<string>) {
    const imageGroup = this.svg.select(".images");
    const imageWidth = 120;
    const imageHeight = 120;
    const imageXOffset = -20;
    const imageYOffset = 5;

    const images = imageGroup.selectAll(".image")
      .data(this.data.values, (d: BarChartEntry) => d.image);

    images.exit()
      .remove();

    const enteredImages = images.enter()
      .append("image")
      .attr("class", (d: BarChartEntry) => "image " + d.id)
      .attr("xlink:href", (d: BarChartEntry) => d.image)
      .attr("x", (d: BarChartEntry) => x(d.x)! + imageXOffset)
      .attr("y", () => this.config.height - (imageHeight + imageYOffset))
      .attr("width", imageWidth)
      .attr("height", imageHeight)
      .attr("cursor", "pointer")
      .on("click", (event: MouseEvent, entry: BarChartEntry) => { this.onImageClick(event, entry); })

    images.merge(enteredImages)
      .attr("x", (d: BarChartEntry) => x(d.x)! + imageXOffset)
      .attr("y", () => this.config.height - (imageHeight + imageYOffset))
      .transition().duration(this.transitionDuration / 2)
      .attr("x", (d: BarChartEntry) => x(d.x)! + imageXOffset)
      .attr("y", () => this.config.height - (imageHeight + imageYOffset))
      .attr("width", imageWidth)
      .attr("height", imageHeight)
      .transition().duration(this.transitionDuration / 2)
      .attr("x", (d: BarChartEntry) => x(d.x)! + imageXOffset)
      .attr("y", () => this.config.height - (imageHeight + imageYOffset))
      .attr("width", imageWidth)
      .attr("height", imageHeight);
    
    if (this.genderStore.selected()) {
      d3.selectAll(".image")
        .classed('selected', false)
        .style("opacity", 0.5);

      d3.select("." + this.genderStore.selected())
        .classed('selected', true)
        .style("opacity", 1);
    }
  }

  private onImageClick(event: MouseEvent, entry: BarChartEntry) {
    if(this.appStore.isLoading()) {
      return
    }

    const target = event.currentTarget as HTMLElement;

    if (target.classList.contains('selected')) {
      target.classList.remove('selected');
      d3.selectAll(".image")
        .transition().duration(200)
        .style("opacity", 1);

      this.selectedSubject.next(null);
    } else {
      d3.selectAll(".image")
        .classed('selected', false)
        .transition().duration(200)
        .style("opacity", 0.5);

      d3.select(target)
        .classed('selected', true)
        .transition().duration(200)
        .attr("height", 125)
        .style("opacity", 1).transition().duration(200).attr("height", 120);

      this.selectedSubject.next(entry.id);
    }
  }

  private drawBars(x: ScaleBand<string>, y: ScaleLinear<number, number, never>) {
    let barsGroup = this.svg.select(".bar-group");
    if (barsGroup.empty()) {
      barsGroup = this.svg.append("g").attr("class", "bar-group");
    }

    // Create and fill the bars
    const bars = barsGroup.selectAll(".bar")
      .data(this.data.values, (d: BarChartEntry) => d.id + d.color);

    // Exit alter Balken
    bars.exit()
      .transition().duration(this.transitionDuration)
      .attr("y", this.config.height)
      .attr("height", 0)
      .remove();

    // Enter neuer Balken
    const enteredBars = bars.enter()
      .append("rect")
      .attr("class", (d: BarChartEntry) => "bar " + d.id)
      .attr("x", (d: BarChartEntry) => x(d.x)! + this.imageOffset)
      .attr("y", this.config.height)
      .attr("width", this.rectWidth)
      .attr("height", 0)
      .attr("stroke", (d: BarChartEntry) => d.color || this.defaultBarColor)
      .attr("stroke-width", 1.25+"px")
      .attr("fill-opacity", 0.25)
      .attr("rx", 5)
      .attr("ry", 5)
      .attr("fill", (d: BarChartEntry) => d.color || this.defaultBarColor)

    // Merge und Update der Balken
    enteredBars.merge(bars)
      .transition().duration(this.transitionDuration)
      .attr("x", (d: BarChartEntry) => x(d.x)! + this.imageOffset)
      .attr("y", (d: BarChartEntry) => y(d.y))
      .attr("width", this.rectWidth)
      .attr("height", (d: BarChartEntry) => this.config.height - y(d.y));
  }

  private drawValueLabels(x: ScaleBand<string>, y: ScaleLinear<number, number, never>) {
    const totalCount = this.data.values.reduce((acc, value) => acc + value.y, 0);

    type BarChartEntryWithPercentage = BarChartEntry & { percentage: number };
    const percentages: BarChartEntryWithPercentage[] = this.data.values.map(v => {
      return {
        ...v,
        percentage: totalCount === 0 ? 0 : v.y / totalCount * 100
      }
    });

    let valueGroup = this.svg.select(".values");
    if (valueGroup.empty()) {
      valueGroup = this.svg.append("g").attr("class", "values");
    }

    const valueLabels = valueGroup.selectAll(".value")
      .data(percentages, (d: BarChartEntryWithPercentage) => d.id + d.color);

    valueLabels.exit()
      .remove();

    const enteredValueLabels = valueLabels.enter()
      .append("text")
      .attr("class", "value")
      .attr("x", (d: BarChartEntryWithPercentage) => x(d.x)! + this.imageOffset + (this.rectWidth / 2))
      .attr("y", (d: BarChartEntryWithPercentage) => y(d.y))
      .style("text-anchor", "middle")
      .style("font-size", "14px")
      .attr("fill", (d: BarChartEntryWithPercentage) => d.color)
      .style("font-weight", "bold");

    enteredValueLabels.append("tspan")
      .attr("class", "percentage")
      .attr("x", (d: BarChartEntryWithPercentage) => x(d.x)! + this.imageOffset + (this.rectWidth / 2))
      .text((d: BarChartEntryWithPercentage) => Math.round(d.percentage) + "%");

    enteredValueLabels.append("tspan")
      .attr("class", "absolute")
      .attr("x", (d: BarChartEntryWithPercentage) => x(d.x)! + this.imageOffset + (this.rectWidth / 2))
      .attr("font-size", "11px")
      .attr("dy", "1.2em")
      .text((d: BarChartEntryWithPercentage) => d.y);

    const mergedLabels = valueLabels.merge(enteredValueLabels);

    mergedLabels.transition().duration(this.transitionDuration)
      .attr("x", (d: BarChartEntryWithPercentage) => x(d.x)! + this.imageOffset + (this.rectWidth / 2))
      .attr("y", (d: BarChartEntryWithPercentage) => y(d.y) - 18);

    mergedLabels.select("tspan.percentage")
      .text((d: BarChartEntryWithPercentage) => Math.round(d.percentage) + "%");

    mergedLabels.select("tspan.absolute")
      .attr("x", (d: BarChartEntryWithPercentage) => x(d.x)! + this.imageOffset + (this.rectWidth / 2))
      .attr("opacity", 0.5)
      .text((d: BarChartEntryWithPercentage) => d.y);
  }
}
