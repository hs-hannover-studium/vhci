import { NgForOf, NgIf } from "@angular/common";
import {
  AfterViewInit,
  Component,
  EventEmitter, inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormControl, ReactiveFormsModule } from "@angular/forms";
import { MatIconButton } from "@angular/material/button";
import { MatFormField, MatLabel } from "@angular/material/form-field";
import { MatIcon } from "@angular/material/icon";
import { MatInput } from "@angular/material/input";
import { MatMenu, MatMenuItem, MatMenuTrigger } from "@angular/material/menu";
import { MatOption, MatSelectModule } from '@angular/material/select';
import { MatSlideToggle } from "@angular/material/slide-toggle";
import * as d3 from "d3";
import { LineRadial, ScaleLinear } from "d3";
import { BehaviorSubject, distinctUntilChanged, Subject, takeUntil } from 'rxjs';

import {AppStore} from "../../../app.store";
import {activeGenres} from '../../../models/animeGenres';
import {ChartConfig} from "../../../models/chart.config";
import {ChartDescription} from "../../../models/chartDescription";
import {ChartHeadingComponent} from "../../chart-heading/chart-heading.component";

import { TooltipComponent } from "../../tooltip/tooltip.component";
import { Chart } from "../bar-chart/bar-chart.component";

export interface RadarChartConfig extends ChartConfig {
  height: number;
  maxValue?: number;
  levels: number;
  roundStrokes: boolean;
  circleOpacity: number;
  areaOpacity: number;
  strokeWidth: number;
  dotRadius: number;
}

export interface RadarChartData {
  values: RadarChartEntry[];
}

export interface RadarChartEntry {
  className: string;
  axisValues: RadarAxisValue[]
  color: string;
}

export interface RadarAxisValue {
  axis: string;
  value: number;
}

@Component({
  selector: 'app-radar-chart',
  standalone: true,
  imports: [
    MatFormField,
    MatIcon,
    MatIconButton,
    MatInput,
    MatLabel,
    MatMenu,
    MatMenuItem,
    MatSlideToggle,
    NgForOf,
    NgIf,
    ReactiveFormsModule,
    MatMenuTrigger,
    ChartHeadingComponent,
    MatOption, MatSelectModule, ReactiveFormsModule, TooltipComponent
  ],
  templateUrl: './radar-chart.component.html',
  styleUrl: './radar-chart.component.scss'
})
export class RadarChartComponent extends Chart implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() config!: RadarChartConfig;
  @Input() data!: RadarChartData;
  @Input() description: ChartDescription = { title: '', description: '' };

  @Input() set disabled(value: boolean) {
    this.isDisabledSubject.next(value);
  };

  @Output() dataChange = new EventEmitter<RadarChartEntry[]>();

  public isDisabledSubject = new BehaviorSubject<boolean>(false);

  private readonly destroySubject = new Subject<void>();

  private transitionDuration = 750;

  public form = this.formBuilder.group({
    genresControl: new FormControl<string[] | null>([]),
    fixGenresControl: new FormControl<boolean>(false)
  });

  public selectedGenres: string[] = [];

  public activeGenres = activeGenres;

  public tooltipContent = { title: '', value: '', value2: '' };
  public tooltipPosition = { x: 0, y: 0 };
  public tooltipVisible = false;

  private store = inject(AppStore);

  constructor(private readonly formBuilder: FormBuilder) {
    super();
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data'] && !changes['data'].firstChange) {
      if (!this.form.controls.fixGenresControl.value && this.data.values.length > 0) {
        const totalCounts = this.data.values.map(x => Object.assign({}, x))[0].axisValues.sort((a, b) => b.value - a.value).slice(0, 6);
        this.selectedGenres = totalCounts.map(item => item.axis);
      }

      this.update(this.selectedGenres);
    }

    if (changes['config'] && !changes['config'].firstChange) {
      this.update(this.selectedGenres);
    }
  }

  ngOnInit(): void {
    this.isDisabledSubject.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((isDisabled) => {
      if (isDisabled) {
        this.form.disable();
      } else {
        this.form.enable();
      }
    });

    this.form.controls.genresControl.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.destroySubject)
    ).subscribe((value) => {
      this.selectedGenres = value ?? [];
      this.update(this.selectedGenres);
    });
  }

  ngAfterViewInit(): void {
    this.createSvg();

    if (this.data.values.length > 0) {
      if (!this.form.controls.fixGenresControl.value) {
        const totalCounts = this.data.values.map(x => Object.assign({}, x))[0].axisValues.sort((a, b) => b.value - a.value).slice(0, 6);
        this.selectedGenres = totalCounts.map(item => item.axis);
      }
    }

    this.update(this.selectedGenres);
  }

  private createSvg() {
    const chart = d3.select(`svg#${this.chartId}`)
      .attr("width", this.config.width + this.config.margin)
      .attr("height", this.config.height + this.config.margin)
      .append("g")
      .attr("transform", "translate(" + ((this.config.width + this.config.margin) / 2) + "," + ((this.config.height + this.config.margin) / 2) + ")");

    //Wrapper for the grid & axes
    const axisWrapper = chart.append("g")
      .attr("class", "axisWrapper")

    axisWrapper.append("g")
      .attr("class", "level-group");

    axisWrapper.append("g")
      .attr("class", "level-label-group");

    axisWrapper.append("g")
      .attr("class", "line-group");

    axisWrapper.append("g")
      .attr("class", "line-label-group");

    const radarGroup = chart.append("g")
      .attr("class", "radar-group");

    radarGroup.append("g")
      .attr("class", "radar-area-group");

    radarGroup.append("g")
      .attr("class", "radar-stroke-group");

    radarGroup.append("g")
      .attr("class", "radar-circle-group");

    this.svg = chart;
  }

  private update(selectedAxes: string[]) {
    const allAxis = new Set(selectedAxes);
    const allAxisNames = [...allAxis];	//Names of each axis

    const data = this.data.values.map(x => Object.assign({}, x)).map(entry => {
      entry.axisValues = entry.axisValues.filter((axisValue) => allAxis.has(axisValue.axis)).sort((a, b) => {
        const sortOrderA = allAxisNames.findIndex(axis => axis == a.axis) || 0;
        const sortOrderB = allAxisNames.findIndex(axis => axis == b.axis) || 0;
        return sortOrderA - sortOrderB;
      });
      return entry;
    });

    let maxValue = data.reduce((max, current) => {
      const currentMax = current.axisValues.reduce((maxValue, axis) => {
        return Math.max(maxValue, axis.value);
      }, 0);
      return Math.max(max ?? 0, currentMax);
    }, this.config.maxValue || 0);
    maxValue = (Math.ceil(maxValue * 200)/ 200) + 0.005;

    if(maxValue === 0 || maxValue > 1) {
      maxValue = 1;
    }

    this.form.controls.genresControl.setValue(allAxisNames, { emitEvent: false });
    const total = allAxisNames.length; //The number of different axes
    const radius = Math.min(this.config.width / 2, this.config.height / 2); //Radius of the outermost circle
    const angleSlice = Math.PI * 2 / total; //The width in radians of each "slice"x

    this.dataChange.emit(data);

    //Scale for the radius
    const rScale = d3.scaleLinear()
      .range([0, radius])
      .domain([0, maxValue]);

    const radarLine = d3.lineRadial<RadarAxisValue>()
      .curve(this.config.roundStrokes ? d3.curveCardinalClosed : d3.curveLinearClosed)
      .radius((d) => rScale(d.value))
      .angle((_, i) => i * angleSlice);

    this.drawLabel(radius, maxValue);
    this.drawLevel(radius);
    this.drawLines(allAxisNames, rScale, maxValue, angleSlice);
    this.drawLineLabels(allAxisNames, rScale, maxValue, angleSlice);
    this.drawArea(data, radarLine);
    this.drawStroke(data, radarLine);
    this.drawCircles(data, rScale, angleSlice);
  }

  private drawArea(data: RadarChartEntry[], radarLine: LineRadial<RadarAxisValue>): void {
    const group = this.svg.select(".radar-area-group");
    const entries = group.selectAll(".radar-area")
      .data(data);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("path")
      .attr("class", "radar-area")
      .attr("d", (d: RadarChartEntry) => radarLine(d.axisValues))
      .style("fill", (d: RadarChartEntry, i: number) => d.color)
      .style("fill-opacity", this.config.areaOpacity)
      .on('mouseover', function (this: HTMLElement) {
        //Dim all blobs
        d3.selectAll(".radar-area")
          .transition().duration(200)
          .style("fill-opacity", 0.1);
        //Bring back the hovered over blob
        d3.select(this)
          .transition().duration(200)
          .style("fill-opacity", 0.7);
      })
      .on('mouseout', () => {
        //Bring back all blobs
        d3.selectAll(".radar-area")
          .transition().duration(200)
          .style("fill-opacity", this.config.areaOpacity);
      });

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("d", (d: RadarChartEntry) => radarLine(d.axisValues))
      .style("fill", (d: RadarChartEntry, i: number) => d.color)
      .style("fill-opacity", this.config.areaOpacity);
  }

  private drawLines(values: string[], rScale: ScaleLinear<number, number, never>, maxValue: number, angleSlice: number) {
    const group = this.svg.select(".line-group");
    const entries = group.selectAll(".line")
      .data(values);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("line")
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", (_: string, i: number) => rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("y2", (_: string, i: number) => rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2))
      .attr("class", "line")
      .style("stroke", "white")
      .style("stroke-width", "2px");

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("x1", 0)
      .attr("y1", 0)
      .attr("x2", (_: string, i: number) => rScale(maxValue * 1.1) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("y2", (_: string, i: number) => rScale(maxValue * 1.1) * Math.sin(angleSlice * i - Math.PI / 2))
      .attr("class", "line")
      .style("stroke", "white")
      .style("stroke-width", "2px");
  }

  private drawLevel(radius: number): void {
    const group = this.svg.select(".level-group");
    const entries = group.selectAll(".level")
      .data(d3.range(1, (this.config.levels + 1)).reverse());

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("circle")
      .attr("class", "level")
      .attr("r", (d: number) => radius / this.config.levels * d)
      .style("fill", "#CDCDCD")
      .style("stroke", this.store.activeTheme.tomoChan.primary)
      .style("fill-opacity", this.config.circleOpacity);

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("r", (d: number) => radius / this.config.levels * d)
      .style("fill", "#CDCDCD")
      .style("stroke", this.store.activeTheme.tomoChan.primary)
      .style("fill-opacity", this.config.circleOpacity);
  }

  private drawLabel(radius: number, maxValue: number) {
    const group = this.svg.select(".level-label-group");
    const entries = group.selectAll(".level-label")
      .data(d3.range(1, (this.config.levels + 1)).reverse());

    entries.exit()
      .remove();

    const entered = entries
      .enter()
      .append("text")
      .attr("class", "level-label")
      .attr("x", 4)
      .attr("y", (d: number) => -d * radius / this.config.levels + 1)
      .attr("dy", "-0.3em")
      .style("font-size", "10px")
      .attr("fill", "#737373")

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("x", 4)
      .attr("y", (d: number) => -d * radius / this.config.levels)
      .attr("dy", "-0.3em")
      .style("font-size", "10px")
      .attr("fill", "#737373")
      .text((d: number) => d3.format('.1%')(Math.round(maxValue * d / this.config.levels  * 1000)/ 1000));
  }

  private drawLineLabels(value: string[], rScale: ScaleLinear<number, number, never>, maxValue: number, angleSlice: number) {
    const group = this.svg.select(".line-label-group");

    const entries = group.selectAll(".line-label")
      .data(value);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("text")
      .attr("class", "line-label")
      .style("font-size", "11px")
      .attr("text-anchor", "middle")
      .attr("dy", "0.35em")
      .attr("x", (_: string, i: number) => rScale(maxValue * 1.25) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("y", (_: string, i: number) => {
        const angle = angleSlice * i - Math.PI / 2;
        const factor = (Math.abs(Math.cos(angle)) < 0.1) ? 1.2 : 1.25;
        return rScale(maxValue * factor) * Math.sin(angle);
      })
      .text((d: string) => d);

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .style("font-size", "11px")
      .attr("text-anchor", "middle")
      .attr("dy", "0.35em")
      .attr("x", (_: string, i: number) => rScale(maxValue * 1.25) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("y", (_: string, i: number) => {
        const angle = angleSlice * i - Math.PI / 2;
        const factor = (Math.abs(Math.cos(angle)) < 0.1) ? 1.2 : 1.25;
        return rScale(maxValue * factor) * Math.sin(angle);
      })
      .text((d: string) => d);
  }

  private drawStroke(data: RadarChartEntry[], radarLine: LineRadial<RadarAxisValue>) {
    const group = this.svg.select(".radar-stroke-group");
    const entries = group.selectAll(".radar-stroke")
      .data(data, (d: RadarChartEntry) => d.className);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("path")
      .attr("class", "radar-stroke")
      .attr("d", (d: RadarChartEntry) => radarLine(d.axisValues))
      .style("stroke-width", this.config.strokeWidth + "px")
      .style("stroke", (d: RadarChartEntry, i: number) => d.color)
      .style("fill", "none");

    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("d", (d: RadarChartEntry) => radarLine(d.axisValues))
      .style("stroke-width", this.config.strokeWidth + "px")
      .style("stroke", (d: RadarChartEntry, i: number) => d.color)
      .style("fill", "none");
  }

  private drawCircles(data: RadarChartEntry[], rScale: ScaleLinear<number, number, never>, angleSlice: number) {
    const group = this.svg.select(".radar-circle-group");

    type AxisValueWithIndex = { color: string, index: number, axis: string, value: number, groupIndex: number};

    const flattenedData: AxisValueWithIndex[] = data.flatMap((entry, groupIndex) => entry.axisValues.map((value, index) => ({
      ...value,
      groupIndex,
      color: entry.color,
      index
    })));

    const entries = group.selectAll(".radar-circle")
      .data(flattenedData, (d: AxisValueWithIndex) => d.color + '-' + d.index);

    entries.exit()
      .remove();

    const entered = entries.enter()
      .append("circle")
      .attr("class", "radar-circle")
      .attr("r", this.config.dotRadius)
      .attr("cx", (d: AxisValueWithIndex, i: number) => rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("cy", (d: AxisValueWithIndex, i: number) => rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2))
      .style("fill", (d: AxisValueWithIndex) => d.color)
      .style("fill-opacity", 0.8).style("pointer-events", "all")
      .style("pointer-events", "all")
      .on("mousemove", (event: MouseEvent, d: AxisValueWithIndex) => {
        const svgRect = this.svg.node().parentNode.getBoundingClientRect();

        // Update tooltip content and position
        if (this.data.values.length > 1) {
          this.tooltipContent = {
            title: this.data.values[d.groupIndex].className,
            value: d.axis,
            value2: d3.format('.2%')(d.value)
          };
        } else {
          this.tooltipContent = {
            title: d.axis,
            value: d3.format('.2%')(d.value),
            value2: ''
          };
        }
        

        this.tooltipPosition = {
          x: event.clientX - svgRect.left + 10,
          y: event.clientY - svgRect.top + 10
        };

        this.tooltipVisible = true;
      })
      .on("mouseout", () => {
        this.tooltipVisible = false;
      });


    entered.merge(entries)
      .transition().duration(this.transitionDuration)
      .attr("r", this.config.dotRadius)
      .attr("cx", (d: AxisValueWithIndex, i: number) => rScale(d.value) * Math.cos(angleSlice * i - Math.PI / 2))
      .attr("cy", (d: AxisValueWithIndex, i: number) => rScale(d.value) * Math.sin(angleSlice * i - Math.PI / 2))
      .style("fill", (d: AxisValueWithIndex) => d.color)
      .style("fill-opacity", 0.8);
  }
}
