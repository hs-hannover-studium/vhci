import * as d3 from "d3";

import {ChoroplethColorScale} from "../../../models/ChoroplethColorScale";
import {UserLocationCount} from "../../../models/user-location.count";

export class ChoroplethMap {
  public min: number;
  public max: number;
  public steps: number;
  public colors!: string[];
  public domain: number[];

  constructor(data: UserLocationCount[], steps: number, colors: ChoroplethColorScale) {
    if (steps > colors.length) {
      throw new Error("The number of steps must be less than or equal to the number of colors");
    }

    this.min = Math.min(...data.map((value) => value.count));
    this.max = Math.max(...data.map((value) => value.count));
    this.steps = steps;

    switch (steps) {
      case 1:
        this.colors = [colors[5]];
        break;
      case 2:
        this.colors = [colors[0], colors[5]];
        break;
      case 3:
        this.colors = [colors[0], colors[3], colors[5]];
        break;
      case 4:
        this.colors = [colors[0], colors[2], colors[4], colors[5]];
        break;
      case 5:
        this.colors = [colors[0], colors[2], colors[3], colors[4], colors[5]];
        break;
      case 6:
        this.colors = colors;
        break;
    }


    let scale = d3.scaleLinear().domain([0, steps]).range([0, 1200]); //linear scale
    scale = d3.scaleLog().domain([1, this.max]).range([0, 1]); // log scale

    this.domain = [...Array.from({length: steps}, (_, i) => Math.floor(scale(i)))]; // for linear scale
    this.domain = [0, ...d3.range(1, steps).map(i => Math.floor(scale.invert(i / steps)))]; // for log scale
  }

  public colorScale() {
    return d3.scaleThreshold<number, string>().domain(this.domain.slice(1,this.steps)).range(this.colors);
  }
}
