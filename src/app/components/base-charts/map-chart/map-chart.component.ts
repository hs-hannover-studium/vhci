import {AsyncPipe, NgForOf, NgIf, NgStyle} from "@angular/common";
import {
  AfterViewInit,
  Component,
  EventEmitter,
  inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {FormBuilder, FormControl, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatAutocomplete, MatAutocompleteTrigger, MatOption} from "@angular/material/autocomplete";
import {MatButton, MatIconButton} from "@angular/material/button";
import {MatIcon} from "@angular/material/icon";
import {MatFormField, MatInput, MatLabel} from "@angular/material/input";
import {MatMenu, MatMenuItem, MatMenuTrigger} from "@angular/material/menu";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import * as d3 from "d3";
import {Feature, FeatureCollection} from 'geojson';
import {isNaN} from "lodash";
import {
  BehaviorSubject,
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  from,
  map,
  Observable,
  startWith,
  Subject,
  switchMap,
  takeUntil
} from "rxjs";

import {ChartConfig} from "../../../models/chart.config";
import {ChartDescription} from "../../../models/chartDescription";
import {ChoroplethColorScale, colorThemes} from "../../../models/ChoroplethColorScale";
import {UserLocationCount} from "../../../models/user-location.count";
import {filterUndefined} from "../../../util/filter-null-or-undefined";
import {ChartHeadingComponent} from "../../chart-heading/chart-heading.component";
import {TooltipComponent} from "../../tooltip/tooltip.component";
import {Chart} from "../bar-chart/bar-chart.component";
import {ChoroplethMap} from "./choropleth.map";
import { GenderStore as GenderStore } from "../../user-gender-count-chart/user-gender-count.store";
import { SelectedStateStore } from "../../user-location-count-chart/selected-state.store";

export interface MapChartData {
  values: UserLocationCount[];
}

export interface MapChartConfig extends ChartConfig {
  customColors?: {
    hoverFillColor: string;
    selectedFillColor: string;
  },
  choropleth?: {
    steps: number;
    colorScale: ChoroplethColorScale;
  }
}

@Component({
  selector: 'app-map-chart',
  standalone: true,
  imports: [
    MatIconButton,
    MatIcon,
    MatMenuTrigger,
    MatMenuItem,
    MatMenu,
    MatSlideToggle,
    FormsModule,
    ReactiveFormsModule,
    NgForOf,
    NgIf,
    NgStyle,
    MatInput,
    MatFormField,
    MatLabel,
    ChartHeadingComponent,
    MatAutocomplete,
    MatOption,
    MatAutocompleteTrigger,
    AsyncPipe,
    MatButton,
    TooltipComponent
  ],
  templateUrl: './map-chart.component.html',
  styleUrl: './map-chart.component.scss'
})
export class MapChartComponent extends Chart implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @Input() config!: MapChartConfig;
  @Input() data!: MapChartData;

  @Input() set disabled(value: boolean) {
    this.isDisabledSubject.next(value);
  };

  @Input() description: ChartDescription = {
    title: "",
    description: ""
  };

  public isDisabledSubject = new BehaviorSubject<boolean>(false);
  private isDisabled = false;

  @Output() selectedState = new EventEmitter<string | null>();

  public form = this.formBuilder.group({
    search: new FormControl<string | null>(null),
    choroplethSteps: new FormControl(6),
  });

  public useChoroplethControl = new FormControl(true);

  private defaultFillColor = '#162040';
  private defaultStrokeColor = 'black';
  private defaultOpacity = 0.5;
  private defaultStrokeWidth = 1.5;
  private selectedStrokeWidth = 1.5;

  private aspectRatio = 1.6;
  public choropleth!: ChoroplethMap | null;

  private selectedSubject = new Subject<string | null>();
  private readonly destroySubject = new Subject<void>();

  public states!: Observable<string[]>;

  private geoJson$: Observable<FeatureCollection> = from(d3.json("assets/json/us-states.json")) as Observable<FeatureCollection>;

  public tooltipContent = {title: '', value: ''};
  public tooltipPosition = {x: 0, y: 0};
  public tooltipVisible = false;

  private refreshSubject = new Subject<void>();

  private genderStore = inject(GenderStore);
  private selectedStateStore = inject(SelectedStateStore);

  constructor(private readonly formBuilder: FormBuilder) {
    super();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['data'] && !changes['data'].firstChange) {
      this.refreshSubject.next();
    }

    if (changes['config'] && !changes['config'].firstChange) {
      this.refreshSubject.next();
    }
  }

  public ngOnInit(): void {
    this.isDisabledSubject.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((isDisabled) => {
      if (isDisabled) {
        this.form.disable();
      } else {
        this.form.enable();
      }

      this.isDisabled = isDisabled;
    });

    this.states = this.geoJson$.pipe(
      map((geoJson) => {
        return geoJson.features.map(f => f.properties!['name'] as string);
      }),
      switchMap((states) => {
        return this.form.controls.search.valueChanges.pipe(
          startWith(''),
          map(val => this.filter(states, val ?? ""))
        );
      })
    );

    this.form.controls.search.valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(250),
      switchMap((searchQuery) => {
        return this.states.pipe(
          map((states) => {
            if (!searchQuery) {
              return null;
            }

            return states.find((state) => state.toLowerCase().startsWith(searchQuery?.toLowerCase()));
          })
        )
      }),
      filterUndefined(),
      takeUntil(this.destroySubject),
    ).subscribe((value) => {
      this.select(value);
    });

    this.selectedSubject
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.destroySubject)
      )
      .subscribe((stateName) => {
        this.selectedState.emit(stateName);
      });

    this.drawChoropleth();
  }

  public ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  public ngAfterViewInit(): void {
    this.createSvg();
    this.update()
  }

  private filter(list: string[], query: string): string[] {
    return list.filter(option =>
      option.toLowerCase().includes(query?.toLowerCase() ?? ""));
  }

  public resetChart() {
    this.useChoroplethControl.setValue(false);
    this.form.controls.choroplethSteps.setValue(this.config.choropleth?.steps ? this.config.choropleth.steps : 6);
    this.resetSelection();
  }

  private createSvg() {
    this.svg = d3.select(`svg#${this.chartId}`)
      .attr("width", (this.config.width + this.config.margin))
      .attr("height", this.getHeight())
      .attr("transform", "translate(" + (this.config.margin/2) + "," + 0 + ")");
  }

  private drawChoropleth() {
    if (this.config.choropleth?.steps) {
      this.form.controls.choroplethSteps.setValue(this.config.choropleth.steps);
    }

    combineLatest([
      this.useChoroplethControl.valueChanges.pipe(startWith(this.useChoroplethControl.value)),
      this.form.controls.choroplethSteps.valueChanges.pipe(startWith(this.form.controls.choroplethSteps.value)),
      this.refreshSubject.pipe(startWith(null))
    ])
      .pipe(takeUntil(this.destroySubject))
      .subscribe(([useChoropleth, steps]) => {
        const stepss = steps ? steps : this.config.choropleth?.steps ? this.config.choropleth.steps : 6;
        const colorScale = this.config.choropleth?.colorScale ? this.config.choropleth.colorScale : colorThemes.blue;

        this.choropleth = useChoropleth ? new ChoroplethMap(this.data.values, stepss, colorScale) : null;

        if (!this.genderStore.selected() && !this.selectedStateStore.selected()) {
          this.resetSelection();
        }
        this.update();
      });
  }

  private drawMap(geoJson: FeatureCollection) {
    this.data.values.forEach((value) => {
      const dataState = value.state;
      const dataAnimeFans = value.count;

      const usState = geoJson.features.find(feature => feature.properties!['name'] === dataState);
      if (usState) {
        usState.properties!['fans'] = dataAnimeFans;
      }
    });

    const geoTransform = d3.geoAlbersUsa().fitSize([this.config.width, this.getHeight()], geoJson);
    const path = d3.geoPath().projection(geoTransform);

    let pathGroup = this.svg.select(".path-group");
    if (pathGroup.empty()) {
      pathGroup = this.svg.append("g").attr("class", "path-group");
    }

    const paths = pathGroup.selectAll("path")
      .data(geoJson.features);

    paths.exit()
      .remove();

    paths
      .enter()
      .append('path')
      .merge(paths)
      .attr("class", (d: Feature) => `state-${this.getFormattedStateName(d.properties!['name'])}`)
      .attr('d', path)
      .attr('fill', (d: Feature) => this.getFillColor(d.properties!['fans']))
      .attr('stroke', this.defaultStrokeColor)
      .attr('stroke-linejoin', 'round')
      .attr('stroke-linecap', 'round')
      .attr("stroke-opacity", this.defaultOpacity)
      .attr('stroke-width', this.defaultStrokeWidth)
      .attr('fans', (d: Feature) => d.properties!['fans'])
      .attr("cursor", "pointer")
      .on("click", (i: MouseEvent, d: Feature) => {
        if (this.isDisabled) return;

        this.onClick(i, d);
      })
      .on("mouseover", (i: MouseEvent, d: Feature) => {
        if (this.isDisabled) return;

        this.onMouseOver(i, d);
      })
      .on("mousemove", (i: MouseEvent) => {
        if (this.isDisabled) return;

        this.onMouseMove(i);
      })
      .on("mouseout", (i: MouseEvent, d: Feature) => {
        if (this.isDisabled) return;

        this.onMouseOut(i, d);
      });

    if (this.selectedStateStore.selected()) {
      this.select(this.selectedStateStore.selected()?.name ?? null);
    }
  }

  private update() {
    this.data.values = this.data.values.filter((value) => !isNaN(value.count) && value.state.length > 0);
    this.geoJson$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((geoJson) => {
      this.drawMap(geoJson);
    });
  }

  private getFillColor(value: number) {
    if (this.choropleth) {
      return this.choropleth.colorScale()(value);
    } else {
      return this.defaultFillColor;
    }
  }

  private onMouseOut(i: MouseEvent, d: Feature) {
    this.tooltipVisible = false;

    d3.select(`svg#${this.chartId} .state-${this.getFormattedStateName(d.properties!['name'])}:not(.selected)`)
      .classed("hovered", false)
      .attr("stroke-opacity", this.defaultOpacity)
      .attr("fill", this.getFillColor(d.properties!['fans']))
      .attr("fill-opacity", 1)
      .attr("stroke", this.defaultStrokeColor)
      .attr("stroke-width", this.defaultStrokeWidth);
  }

  private onMouseMove(i: MouseEvent) {
    const svgRect = this.svg.node().parentNode.getBoundingClientRect();

    this.tooltipPosition = {
      x: i.clientX - svgRect.left,
      y: i.clientY - svgRect.top
    };
  }

  private onClick(i: MouseEvent, d: Feature) {
    const stateName = d.properties!['name'];
    if (d3.select(`svg#${this.chartId} .state-${this.getFormattedStateName(stateName)}`).classed('selected')) {
      this.resetSelection();
    } else {
      this.form.controls.search.setValue(stateName);

      this.select(stateName);
    }
  }

  private select(stateName: string | null) {
    this.resetSelectionStyle();

    this.selectedSubject.next(stateName);

    if (!stateName) {
      return;
    }

    const selectedFillColor = this.config.customColors ? this.config.customColors.selectedFillColor : this.defaultFillColor;

    d3.select(`svg#${this.chartId} .state-${this.getFormattedStateName(stateName)}`)
      .classed("selected", true)
      .attr("stroke-opacity", 1)
      .attr("fill", selectedFillColor)
      .attr("fill-opacity", 0.6)
      .attr("stroke", selectedFillColor)
      .attr("stroke-width", this.selectedStrokeWidth);
  }

  private onMouseOver(_: MouseEvent, d: Feature) {
    this.tooltipVisible = true;

    this.hover(d.properties!['name']);

    this.tooltipContent = {
      title: d.properties!['name'],
      value: d.properties!['fans']
    };
  }

  private hover(stateName: string) {
    const hoveredFillColor = this.config.customColors ? this.config.customColors.hoverFillColor : this.defaultFillColor;

    d3.select(`svg#${this.chartId} .state-${this.getFormattedStateName(stateName)}:not(.selected)`)
      .classed("hovered", true)
      .attr("stroke-opacity", 1)
      .attr("fill", hoveredFillColor)
      .attr("fill-opacity", 0.1)
      .attr("stroke", hoveredFillColor)
      .attr("stroke-width", this.selectedStrokeWidth)
  }

  private resetSelection() {
    this.form.controls.search.setValue(null);
    this.resetSelectionStyle();
  }

  private resetSelectionStyle() {
    const selected = d3.selectAll(`svg#${this.chartId} .selected`);

    if (selected.empty()) {
      return;
    }

    const fans = selected.attr("fans");

    selected.classed("selected", false)
      .attr("stroke-opacity", this.defaultOpacity)
      .attr("fill", this.getFillColor(parseInt(fans, 10)))
      .attr("fill-opacity", 1)
      .attr("stroke", this.defaultStrokeColor)
      .attr("stroke-width", this.defaultStrokeWidth);
  }

  private getFormattedStateName(stateName: string) {
    return stateName.trim().replace(/\s/g, "-").replace(/\./g, "-").toLowerCase();
  }

  private getHeight() {
    return this.config.width / this.aspectRatio;
  }
}
