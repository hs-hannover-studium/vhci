import {signalStore, withState} from '@ngrx/signals';

interface AverageAgeByGender {
  median: number;
}

type AverageAgeByGenderState = {
  selected: AverageAgeByGender | null;
};

const initialState: AverageAgeByGenderState = {
  selected: null
};

export const AverageAgeByGenderStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
);
