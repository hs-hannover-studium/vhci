import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageAgeByGenderChartComponent } from './average-age-by-gender-chart.component';

describe('AverageAgeByGenderChartComponent', () => {
  let component: AverageAgeByGenderChartComponent;
  let fixture: ComponentFixture<AverageAgeByGenderChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AverageAgeByGenderChartComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AverageAgeByGenderChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
