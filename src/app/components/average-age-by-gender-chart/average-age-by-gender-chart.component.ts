import {AsyncPipe, JsonPipe, NgIf} from "@angular/common";
import {
  Component,
  computed,
  inject,
  OnDestroy,
  Signal,
} from '@angular/core';
import {toObservable} from "@angular/core/rxjs-interop";
import {getState, patchState} from "@ngrx/signals";
import {isNaN} from "lodash";
import {Observable, Subject, takeUntil} from "rxjs";

import {AppStore} from "../../app.store";
import {ChartDescription} from "../../models/chartDescription";
import {User} from "../../models/user";
import {
  BarChartComponent,
} from "../base-charts/bar-chart/bar-chart.component";
import {
  BoxPlotChartComponent,
  BoxPlotConfig,
  BoxPlotData
} from "../base-charts/box-plot-chart/box-plot-chart.component";
import {GenderStore} from "../user-gender-count-chart/user-gender-count.store";
import {SelectedStateStore} from "../user-location-count-chart/selected-state.store";
import {AverageAgeByGenderStore} from "./average-age-by-gender.store";
import { GenderType } from "../../models/genderType";

@Component({
  selector: 'app-average-age-by-gender-chart',
  standalone: true,
  imports: [
    AsyncPipe,
    BarChartComponent,
    NgIf,
    BoxPlotChartComponent,
    JsonPipe
  ],
  templateUrl: './average-age-by-gender-chart.component.html',
  styleUrl: './average-age-by-gender-chart.component.scss'
})
export class AverageAgeByGenderChartComponent implements OnDestroy {
  public boxPlotChartConfig: BoxPlotConfig = {
    margin: 70,
    width: 200,
    height: 205,
    title: "Age by gender",
    domain: {min: 0, max: 80, maxOffset: 5},
  }

  public description: ChartDescription = {
    title: "Gender count",
    description: "This chart shows the age distribution for the given genders. It shows the min, max and median age of each gender." +
      "<br><br><b>Interactions</b>: " +
      "<ul>" +
      "<li>None</li>" +
      "</ul>" +
      "<br><b>Goal</b>: " +
      "Get insights into the age distribution of anime fans."
  }

  private readonly store = inject(AverageAgeByGenderStore);
  private readonly selectedStateStore = inject(SelectedStateStore);
  private readonly genderStore = inject(GenderStore)
  private readonly appStore = inject(AppStore);

  private selectedTheme$ = toObservable(this.appStore.activeTheme);

  protected theme = getState(this.appStore).activeTheme;

  private readonly dataStateSignal: Signal<User[]>;
  private dataState$: Observable<User[]>;

  private stateFilter: string | null = null;

  private readonly selectedStateSignal: Signal<string | null>;
  private selectedState$: Observable<string | null>

  private genderFilter: GenderType | null = null;

  private readonly selectedGenderSignal: Signal<GenderType | null>;
  private selectedGender$: Observable<GenderType | null>;

  public dataSubject = new Subject<BoxPlotData>();
  public chartData$ = this.dataSubject.asObservable();
  private destroySubject = new Subject<void>();
  private users: User[] = [];

  constructor() {
    // To filter by state
    this.selectedStateSignal = computed(() => {
      return this.selectedStateStore.selected()?.name ?? null;
    });

    this.selectedState$ = toObservable(this.selectedStateSignal);

    this.selectedState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((state) => {
      this.stateFilter = state;
      this.createChartData();
    });

    // To filter by gender
    this.selectedGenderSignal = computed(() => {
      return this.genderStore.selected() ?? null;
    });

    this.selectedGender$ = toObservable(this.selectedGenderSignal);

    this.selectedGender$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((gender) => {
      this.genderFilter = gender;
      this.createChartData();
    });

    // To update the chart when the data changes
    this.dataStateSignal = computed(() => {
      return getState(this.appStore).users;
    });

    this.dataState$ = toObservable(this.dataStateSignal);

    this.dataState$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((users) => {
      this.users = users;
      this.createChartData();
    });

    this.selectedTheme$.pipe(
      takeUntil(this.destroySubject)
    ).subscribe((theme) => {
      this.theme = theme;
      this.createChartData();
    });
  }

  ngOnDestroy(): void {
    this.destroySubject.next();
    this.destroySubject.complete();
  }

  private createChartData() {
    let users = this.users.filter((user: User) => !isNaN(user.age));

    if (this.stateFilter) {
      users = users.filter((user: User) => {
        return user.country === this.stateFilter;
      });
    }

    if (this.genderFilter) {
      users = users.filter((user: User) => {
        return user.gender === this.genderFilter;
      });
    }

    const gender = this.genderStore.selected() ?? this.genderStore.dominant();
    const average = Math.round(this.calculateAverageAge(users.filter((user: User) => {
      return user.gender === gender;
    })))

    patchState(this.store, {
      selected: {
        median: average,
      }
    });

    this.dataSubject.next({
      values: users
        .sort((a, b) => a.gender.localeCompare(b.gender))
        .map(user => {
            let color = this.theme.genders.male;

            switch (user.gender) {
              case "Male":
                color = this.theme.genders.male;
                break;
              case "Female":
                color = this.theme.genders.female;
                break;
              case "Non-Binary":
                color = this.theme.genders.nonBinary;
                break;
            }

            return {
              id: user.id,
              x: user.gender,
              y: user.age,
              color: color
            }
          }
        )
    });
  }

  private calculateAverageAge(userList: User[]): number {
    return userList.reduce((acc, user) => acc + user.age, 0) / userList.length;
  }
}
