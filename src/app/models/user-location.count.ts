export interface UserLocationCount {
  state: string;
  count: number;
}
