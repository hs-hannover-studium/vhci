export const allGenres = ['Action', 'Adventure', 'Cars', 'Comedy', 'Dementia', 'Demons',
    'Drama', 'Ecchi', 'Fantasy', 'Game', 'Harem', 'Hentai',
    'Historical', 'Horror', 'Josei', 'Kids', 'Magic', 'Martial Arts',
    'Mecha', 'Military', 'Music', 'Mystery', 'Parody', 'Police',
    'Psychological', 'Romance', 'Samurai', 'School', 'Sci-Fi',
    'Seinen', 'Shoujo', 'Shoujo Ai', 'Shounen', 'Shounen Ai',
    'Slice of Life', 'Space', 'Sports', 'Super Power', 'Supernatural',
    'Thriller', 'Vampire', 'Yaoi', 'Yuri'];

export const activeGenres = ['Action', 'Adventure', 'Comedy', 'Demons',
    'Drama', 'Ecchi', 'Fantasy', 'Game', 'Harem',
    'Historical', 'Horror', 'Josei', 'Magic', 'Martial Arts',
    'Music', 'Mystery', 'Parody',
    'Psychological', 'Romance', 'School', 'Sci-Fi',
    'Seinen', 'Shoujo', 'Shoujo Ai', 'Shounen', 'Shounen Ai',
    'Slice of Life', 'Sports', 'Super Power', 'Supernatural',
    'Thriller', 'Vampire'];