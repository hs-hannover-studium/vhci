export type ChoroplethColorScale = [string, string, string, string, string, string];

export const red: ChoroplethColorScale = [
  "#fff5f0",
  "#fee0d2",
  "#fcbba1",
  "#fc9272",
  "#fb6a4a",
  "#de2d26"
];

const green: ChoroplethColorScale = [
  "#f7fcf5",
  "#e5f5e0",
  "#c7e9c0",
  "#a1d99b",
  "#74c476",
  "#238b45"
];

const blue: ChoroplethColorScale = [
  "#f7fbff",
  "#deebf7",
  "#c6dbef",
  "#9ecae1",
  "#6baed6",
  "#2171b5"
];

const grey: ChoroplethColorScale = [
  "#f7f7f7",
  "#cccccc",
  "#969696",
  "#636363",
  "#252525",
  "#000000"
]

export const colorThemes = {
  blue,
  red,
  green,
  grey
}
