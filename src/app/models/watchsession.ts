export interface Watchsession {
  userId: string;
  animeId: string;
  foodId: string;
  startTime: string;
  endTime: string;
}
