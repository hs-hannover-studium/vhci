export interface UserGenre {
  id: string;
  name: string;
  gender: string;
  country: string;
  genres: string[];
}
