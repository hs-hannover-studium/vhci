import { GenderType } from "./genderType";

export interface User {
  id: string;
  name: string;
  age: number;
  gender: GenderType;
  country: string;
  genres: Map<string, number>;
}
