export type ChartDescription = {
  title: string;
  description: string;
};
