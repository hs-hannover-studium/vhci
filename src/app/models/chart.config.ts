export interface ChartConfig {
  margin: number;
  width: number;
  title: string;
}
