import tinycolor from "tinycolor2";

import {darkColorScale} from "./darkColorScale";
import {Theme} from "./theme";
import {themeAssets} from "./themeAssets";

export const lollipop : Theme = {
  name: 'Lollipop',
  genders: {
    male: '#0795C2',
    female: '#F3186F',
    nonBinary: '#2aa653',
  },
  choropleth: {
    hover: '#401621',
    selected: '#401621',
    colorScale: [
      ...darkColorScale('#162040')
    ]
  },
  barChart: {
    male: `${themeAssets}/lollipop/male.png`,
    female: `${themeAssets}/lollipop/female.png`,
    nonBinary: `${themeAssets}/lollipop/non-binary.png`,
  },
  tomoChan: {
    primary: "#162040",
    secondary: "#e6ebfb",
    text: tinycolor("#162040").lighten(45).toString(),
    happy: `${themeAssets}/lollipop/tomo-chan-happy.png`,
    sad: `${themeAssets}/lollipop/tomo-chan-sad.png`,
  }
}
