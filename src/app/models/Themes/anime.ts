import tinycolor from "tinycolor2";

import {darkColorScale} from "./darkColorScale";
import {Theme} from "./theme";
import {themeAssets} from "./themeAssets";

export const anime: Theme = {
  name: 'Anime',
  genders: {
    male: '#36bcc9', //'#2874A6'
    female: '#E74C3C',
    nonBinary: '#8E44AD',
  },
  choropleth: {
    hover: '#F39C12',
    selected: '#F1C40F',
    colorScale: [
      ...darkColorScale('#48165d')
    ]
  },
  barChart: {
    male: `${themeAssets}/anime/male.png`,
    female: `${themeAssets}/anime/female.png`,
    nonBinary: `${themeAssets}/anime/non-binary.png`,
  },
  tomoChan: {
    primary: "#48165d",
    secondary: tinycolor("48165d").lighten(70).toString(),
    text: tinycolor("48165d").lighten(20).toString(),
    happy: `${themeAssets}/anime/tomo-chan-happy.png`,
    sad: `${themeAssets}/anime/tomo-chan-sad.png`,
  }
}
