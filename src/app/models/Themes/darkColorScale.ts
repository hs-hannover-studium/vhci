import tinycolor from "tinycolor2";

import {ChoroplethColorScale} from "../ChoroplethColorScale";

export function darkColorScale(color: string): ChoroplethColorScale {
  return [
    tinycolor(color).lighten(80).toString(),
    tinycolor(color).lighten(70).toString(),
    tinycolor(color).lighten(55).toString(),
    tinycolor(color).lighten(35).toString(),
    tinycolor(color).lighten(15).toString(),
    color
  ]
}
