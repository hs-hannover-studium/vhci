import {ChoroplethColorScale} from "../ChoroplethColorScale";
import {anime} from "./anime";
import {lollipop} from "./lollipop";

export type Theme = {
  name: string;
  barChart: { female: string; nonBinary: string; male: string };
  choropleth: { hover: string; colorScale: ChoroplethColorScale; selected: string };
  tomoChan: { primary: string, secondary: string, text: string, happy: string, sad: string };
  genders: { female: string; nonBinary: string; male: string }
}

export const registeredThemes = {
  lollipop,
  anime
}

